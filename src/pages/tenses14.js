import React, { Component } from 'react';
import {View, StyleSheet, Text, FlatList} from 'react-native';

class tenses14 extends Component{
    constructor(props){
        super(props);
        this.state = {
            yana : [
                {judul:<Text style={{alignSelf:'center',fontWeight:'bold',fontSize:25,marginBottom:20}}>PAST FUTURE CONTINUOUS TENSE </Text>,nomor:1},
                {judul:<Text style={{marginLeft:5}}>Past future continuous tense atau di sebut juga Past future progressive adalah bentuk kalimat suatu kejadian/keadaan pada waktu yang akan sedang terjadi pada waktu lampau. Dalam penggunaanya past future continuous tense di gunakan untuk menggambarkan atau menjelaskan peristiwa atau keadaan dalam imajinasi kita pada waktu yang akan sedang terjadi di masa lalu, dan hal tersebut belum selesai dilakukan.</Text>,nomor:2},
                {judul:<Text style={{alignSelf:'center',fontSize:20,marginTop:10}}>Rumus Past Future Continuous Tense</Text>,nomor:3},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>   Pembentukan rumus past future continuous tense ini terdiri dari tiga bentuk kalimat, yaitu past tense (verb-2), future tense (Will+verb-1), dan continuous tense (be + verb-ing). Hasil dari penggabungan tiga bentuk tersebut maka mengasilkan rumus past future continuous tense sebagai berikut S + would + be + verb-1+ing.</Text>,nomor:4},
                {judul:<Text style={{marginLeft:5,marginTop:10,fontWeight:'bold'}}>1.       Kalimat Positif</Text>,nomor:5},
                {judul:<Text style={{alignSelf:'center', fontWeight:'bold', marginTop:10}}>S + should/would + V-ing + O</Text>,nomor:6},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>       Contoh:</Text>,nomor:7},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>a.     Uncle Greg should be working hard. (Paman Greg akan sedang bekerja keras.)</Text>,nomor:8},
                {judul:<Text style={{marginLeft:5,marginTop:10,fontWeight:'bold'}}>2.       Kalimat Negatif</Text>,nomor:9},
                {judul:<Text style={{alignSelf:'center', fontWeight:'bold', marginTop:10}}>S + should/would + not + V-ing + O</Text>,nomor:10},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>       Contoh:</Text>,nomor:11},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>a.     Uncle Greg should not be working hard. (Paman Greg tidak akan sedang bekerja keras.)</Text>,nomor:12},
                {judul:<Text style={{marginLeft:5,marginTop:10,fontWeight:'bold'}}>3.       Kalimat Introgatif</Text>,nomor:13},
                {judul:<Text style={{alignSelf:'center', fontWeight:'bold', marginTop:10}}>Should/would + S + V-ing + O?</Text>,nomor:14},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>       Contoh:</Text>,nomor:15},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>a.     Should Uncle Greg be working hard?(Akankah paman Greg sedang bekerja keras? </Text>,nomor:16},
                {judul:<Text style={{alignSelf:'center',fontSize:20,marginTop:10}}>Fungsi Past Future Continuous Tense</Text>,nomor:17},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>Fungsi past future continuous tense dapat digunakan untuk beberapa keadaan atau kejadian, yaitu sebagai berikut:</Text>,nomor:18},
                {judul:<Text style={{marginLeft:5,marginTop:10,fontWeight:'bold'}}>1.       Menyatakan suatu peristiwa atau kejadian yang akan terjadi pada waktu yang akan sedang terjadi pada masa lampau.</Text>,nomor:19},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>       Contoh kalimat:</Text>,nomor:20},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>a.     I should be finishing this job. (Aku akan sedang menyelesaikan pekerjaan ini.)</Text>,nomor:21},
                {judul:<Text style={{marginLeft:5,marginTop:10,fontWeight:'bold'}}>2.       Menyatakan suatu peristiwa atau kejadian yang seharusnya sedang terjadi di masa lampau. Untuk kata kerja bantu yang dipakai dalam bentuk ini adalah kata kerja bantu Would.</Text>,nomor:22},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>       Contoh kalimat:</Text>,nomor:23},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>a.     I would be cleaning my room in the morning yesterday. (Aku seharusnya sedang membersihkan kamarku di pagi hari kemarin.)</Text>,nomor:24},
                {judul:<Text style={{alignSelf:'center',fontSize:20,marginTop:10}}>Ciri-ciri Past Future Continuous Tense</Text>,nomor:25},
                {judul:<Text style={{marginLeft:5,marginTop:10,fontWeight:'bold'}}>1.       Predikat kalimat yang sering dipakai dalam bentuk tense ini sering berupa: S + Should/Would + Present Participle.</Text>,nomor:26},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>       Contoh kalimat:</Text>,nomor:27},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>a.     I should be visiting Aunt Mary’s house. (Aku akan sedang mengunjungi rumah Bibi Mary.)</Text>,nomor:28},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>b.     My cat should be sleeping here. (Kucingku akan sedang tidur di sini.)</Text>,nomor:29},
                {judul:<Text style={{marginLeft:5,marginTop:10,fontWeight:'bold'}}>2.     Kata be diletakkan sebelah kata kerja bantu should/would serta ditambah Present Participle yang berarti sedang.</Text>,nomor:30},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>       Contoh kalimat:</Text>,nomor:31},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>a.     We should be sending the package for our friends. (Aku akan sedang mengirim paket untuk teman-teman kami.)</Text>,nomor:32},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>   Past future continuous tense adalah kalimat yang memiliki fungsi untuk menyatakan rencana sesuatu pada waktu yang akan sedang terjadi di waktu lampau, ciri-cirinya adalah predikat kalimat yang sering dipakai berupa: S + Should/Would + Present Participle.</Text>,nomor:33},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>   Bentuk kalimat past future continuous tense dalam bentuk positif memiliki rumus S + should/would + V-ing + O dengan keterangan waktu yang biasa digunakan tomorrow, next month, the day before, atau on august last year.</Text>,nomor:34},    
            ]
        }
    }
    render(){
        return(
            <View>
                <FlatList
                data = {this.state.yana}
                renderItem = {({item,index}) => (
                    <View>
                        {item.judul}
                    </View>
                )}
                keyExtractor = {(item) => item.nomor}
                />
            </View>
        )
    }
}

export default tenses14;