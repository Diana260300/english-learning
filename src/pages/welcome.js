import React, {Component} from 'react';
import {Image, StyleSheet, View, Text, TouchableOpacity,FlatList, Dimensions} from 'react-native';

const numColumns = 1;
const WIDTH = Dimensions.get('window').width;

class WelcomeAuth extends Component {
    constructor(props) {
        super(props);
        this.state = {
            yana :[
                {menu:<TouchableOpacity onPress={() => this.props.navigation.navigate('Vocabulary')}><Image source={require('../assets/icon2.png')} style={styles.foto}/></TouchableOpacity>,nomor:1},
                {menu:<TouchableOpacity onPress={() => this.props.navigation.navigate('Phrases')}><Image source={require('../assets/icon4.png')} style={styles.foto}/></TouchableOpacity>,nomor:2},
                {menu:<TouchableOpacity onPress={() => this.props.navigation.navigate('Tenses')}><Image source={require('../assets/icon3.png')} style={styles.foto}/></TouchableOpacity>,nomor:3},
            ],
        };
    }
    render(){
    return(
        <View style={{flex:1}}>
            <View style={styles.wrapper}>
                <Text style ={{fontSize : 20,color: "papayawhip", textAlign:"center", marginTop:30}}> Belajar Bahasa Inggris Mudah dan Menyenangkan </Text>
            </View>
           <FlatList
                data = {this.state.yana}
                renderItem = {({item,index}) => (
                    <View style={styles.ukuran}>
                        {item.menu}
                    </View>
                )}
                keyExtractor = {(item) => item.nomor}
                numColumns = {numColumns}
           />
        </View>
    );
    };
};

export default WelcomeAuth;

const styles=StyleSheet.create({
    wrapper: {
        backgroundColor: 'dimgray',
        flex: 1,
    },
    foto: {
        width:300,
        height:150,
    },
    ukuran : {
        height: WIDTH/(numColumns * 2),
        margin: 5,
        padding: 5,
        borderRadius: 5,
        justifyContent:'center',
        alignItems:'center',
    }
});