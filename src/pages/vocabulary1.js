import React, { Component } from 'react';
import {View, StyleSheet, Text,FlatList} from 'react-native';



class vocabulary1 extends Component{
    constructor(props){
        super(props);
        this.state = {
            tugas : [
                {teks:<Text style={styles.modif1}>1.    Always      :   Selalu  </Text>,nomor:1},
                {teks:<Text style={styles.modif2}>2.    After       :   Setelah </Text>,nomor:2},
                {teks:<Text style={styles.modif1}>3.    Accompany   :   Menemani </Text>,nomor:3},
                {teks:<Text style={styles.modif2}>4.    Affection   :   Kasih Sayang </Text>,nomor:4},
                {teks:<Text style={styles.modif1}>5.    Afraid      :   Takut </Text>,nomor:5},
                {teks:<Text style={styles.modif2}>6.    Another     :   Yang Lain </Text>,nomor:6},
                {teks:<Text style={styles.modif1}>7.    Anywhere    :   Di Manapun </Text>,nomor:7},
                {teks:<Text style={styles.modif2}>8.    Arrogant    :   Sombong </Text>,nomor:8},
                {teks:<Text style={styles.modif1}>9.    Autumn      :   Musim Gugur </Text>,nomor:9},
                {teks:<Text style={styles.modif2}>10.   Ant         :   Semut </Text>,nomor:10},
                {teks:<Text style={styles.modif1}>11.   About       :   Tentang</Text>,nomor:11},
                {teks:<Text style={styles.modif2}>12.   Above       :   Di Atas</Text>,nomor:12},
                {teks:<Text style={styles.modif1}>13.   Abroad      :   Di Luar Negeri</Text>,nomor:13},
                {teks:<Text style={styles.modif2}>14.   Accept      :   Menerima</Text>,nomor:14},
                {teks:<Text style={styles.modif1}>15.   Accident    :   Kecelakaan</Text>,nomor:15},
                {teks:<Text style={styles.modif2}>16.   Abuse       :   Penyalahgunaan</Text>,nomor:16},
                {teks:<Text style={styles.modif1}>17.   Accidental  :   Kebetulan</Text>,nomor:17},
                {teks:<Text style={styles.modif2}>18.   Acording To :   Menurut</Text>,nomor:18},
                {teks:<Text style={styles.modif1}>19.   Accuse      :   Menuduh</Text>,nomor:19},
                {teks:<Text style={styles.modif2}>20.   Achieve     :   Mencapai</Text>,nomor:20},
                {teks:<Text style={styles.modif1}>21.   Beloved     :   Tecinta </Text>,nomor:21},
                {teks:<Text style={styles.modif2}>22.   Better      :   Lebih Baik</Text>,nomor:22},
                {teks:<Text style={styles.modif1}>23.   Big         :   Besar </Text>,nomor:23},
                {teks:<Text style={styles.modif2}>24.   Bitter      :   Pahit</Text>,nomor:24},
                {teks:<Text style={styles.modif1}>25.   Bland       :   Lunak </Text>,nomor:25},
                {teks:<Text style={styles.modif2}>26.   Before      :   Sebelum</Text>,nomor:26},
                {teks:<Text style={styles.modif1}>27.   Behind      :   Di Belakang</Text>,nomor:27},
                {teks:<Text style={styles.modif2}>28.   Brain       :   Otak</Text>,nomor:28},
                {teks:<Text style={styles.modif1}>29.   Bridge      :   Jembatan</Text>,nomor:29},
                {teks:<Text style={styles.modif2}>30.   Bread       :   Roti</Text>,nomor:30},
            ],
        }
    }
    render(){
        return(
            <View>
                <FlatList
                data = {this.state.tugas}
                renderItem = {({item,index}) => (
                    <View>
                        {item.teks}
                    </View>
                )}
                keyExtractor = {(item) => item.nomor}
                />
            </View>
        )
    }
};
const styles = StyleSheet.create({
    modif1:{
        fontSize:20, borderColor:'black', width:400, height:50, paddingHorizontal:10,
        borderRadius:5, marginTop:15, backgroundColor:'papayawhip', color:'black',
        paddingTop:10,marginHorizontal:10,
    },
    modif2:{
        fontSize:20, borderColor:'black', width:400, height:50, paddingHorizontal:10,
        borderRadius:5, marginTop:15, backgroundColor:'gray', color:'black',
        paddingTop:10,marginHorizontal:10,
    },
})
export default vocabulary1;
