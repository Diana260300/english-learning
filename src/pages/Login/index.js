import React, {Component} from "react";
import {StyleSheet,Text,View,TouchableOpacity,Alert,Image} from "react-native";
import {InputL} from '../../component';
import FIREBASE from '../../config/FIREBASE';

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      UserName: '',
      Password: '',
    };
  }
  onChangeText = (nameState, value) =>{
    this.setState({
      [nameState] : value
    });
  };
  onSubmit = () => {
    if(this.state.UserName && this.state.Password) {
      const loginreferensi = FIREBASE.database().ref('Login');
      const login = {
        username: this.state.UserName,
        password: this.state.Password
      }
      loginreferensi
      .push(login)
      .then((data) => {
        Alert.alert('sukses', 'login berhasil');
        this.props.navigation.replace('welcome');
      })
      .catch((error) => {
        console.log("Error : ", error);
      })
    }else{
      Alert.alert('Error', 'Username dan Password wajib di isi');
    }
  };
 render (){
    return (
      <View style={styles.container}>
        <View style={styles.page}>
        <Image source={require('../../assets/ikon1.png')} style ={{height:200,width:200}} />
        <InputL
        placeholder={'UserName'}
        keyType="email-address"
        onChangeText={this.onChangeText}
        value={this.state.UserName}
        nameState='UserName'
        />
        <InputL 
        
        placeholder={'Password'}
        onChangeText={this.onChangeText}
        value={this.state.Password}
        nameState='Password'
        />
        <TouchableOpacity style={{borderColor:'black',
        width:100,height:50,
        paddingHorizontal:10,
        borderRadius:15,
        marginTop:15,
        backgroundColor:'black',
        color:'white'
        }} onPress={() => this.onSubmit()}>
          <Text style={{textAlign:'center',fontSize:20,color:'white',marginTop:9}}>Login</Text>
        </TouchableOpacity>
        </View>
      </View>
    )
  }
}
export default Login;

const styles = StyleSheet.create({
  container: {
    flex:1,
    backgroundColor:"bisque",
    alignItems:"center",
    justifyContent:"center",
    flexDirection: 'column'
    },
  inputText:{
    height: 50,
    color:'white'
},
page:{
  borderColor: 'white',
  width: 380,height:470,
  paddingHorizontal: 5,
  borderRadius: 3,
  backgroundColor: 'lightgoldenrodyellow',
  justifyContent:'center',
  alignItems:'center',
},
}); 
