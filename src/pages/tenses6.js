import React, { Component } from 'react';
import {View, StyleSheet, Text,FlatList} from 'react-native';

class tenses6 extends Component{
    constructor(props){
        super(props);
        this.state = {
            yana : [
                {judul:<Text style={{alignSelf:'center',fontWeight:'bold',fontSize:25,marginBottom:20}}>PAST CONTINUOUS TENSE</Text>,nomor:1},
                {judul:<Text style={{marginLeft:5}}>Past continuous tense adalah bentuk tense yang digunakan untuk menyatakan bahwa suatu aksi sedang berlangsung selama waktu tertentu pada waktu lampau (past). Bentuk ini menunjukkan bahwa aksi tersebut terjadi sebelum (began before), selama (was in progress during), dan mungkin berlanjut setelah (continued after) waktu atau aksi lainnya yang terjadi pada masa lampau.</Text>,nomor:2},
                {judul:<Text style={{alignSelf:'center',fontSize:20,marginTop:10}}>Rumus Past Continuous Tense</Text>,nomor:3},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>Past continuous tense dibentuk dengan auxiliary verb “be”, berupa was atau were. Hal ini dikarenakan past tense (terjadi di masa lalu), dan present participle. Was untuk singular subject berupa singular noun (seperti: Andi, book, dan cat) dan singular pronoun (seperti: I, she, he, dan it) kecuali “you”. Sebaliknya were yang merupakan plural verb digunakan pada plural subject seperti plural noun (seperti: cats, people, books) dan plural pronoun (seperti: you, they, we, dan cats), dan you.</Text>,nomor:4},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>Kalimat di dalam past continuous tense dibagi menjadi tiga, yaitu: kalimat postif, kalimat negatif, dan kalimat introgatif.</Text>,nomor:5},
                {judul:<Text style={{marginLeft:5,marginTop:10, fontWeight:'bold'}}>1.      Kalimat positif</Text>,nomor:6},
                {judul:<Text style={{alignSelf:'center', fontWeight:'bold', marginTop:10}}>S + be (was/were) + present participle (-ing). </Text>,nomor:7},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>       Contoh Kalimat: </Text>,nomor:8},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>1.     He was sleeping</Text>,nomor:9},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>2.     The people were waiting</Text>,nomor:10},
                {judul:<Text style={{marginLeft:5,marginTop:10, fontWeight:'bold'}}>2.         Kalimat negatif </Text>,nomor:11},
                {judul:<Text style={{alignSelf:'center', fontWeight:'bold', marginTop:10}}>S + be (was/were) + not + present participle (-ing).</Text>,nomor:12},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>       Contoh Kalimat:</Text>,nomor:13},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>1.     He wasn’t sleeping</Text>,nomor:14},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>2.     The people weren’t waiting</Text>,nomor:15},
                {judul:<Text style={{marginLeft:5,marginTop:10, fontWeight:'bold'}}>3.        Kalimat introgatif</Text>,nomor:16},
                {judul:<Text style={{alignSelf:'center', fontWeight:'bold', marginTop:10}}>be (was/were) + S + present participle (-ing)?</Text>,nomor:17},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>       Contoh Kalimat:</Text>,nomor:18},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>1.     Was he sleeping?</Text>,nomor:19},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>2.     Were the people waiting?</Text>,nomor:20},
                {judul:<Text style={{alignSelf:'center',fontSize:20,marginTop:10}}>Fungsi continuous Tense</Text>,nomor:21},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>ada beberapa fungsi past continuous tense yang perlu kamu ketahui. Di antaranya adalah:</Text>,nomor:22},
                {judul:<Text style={{marginLeft:5,marginTop:10, fontWeight:'bold'}}>A.        Untuk mengindikasikan suatu aksi yang terjadi selama momen tertentu di masa lampau. Adapun keterangan waktu atau time expression yang dapat digunakan adalah: at this time yesterday, at 5 am this morning, all day yesterday, in July. </Text>,nomor:23},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>       Contoh Kalimat:</Text>,nomor:24},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>1.     I was having dinner at this time yesterday. (Saya sedang makan malam pada jam ini kemarin.)</Text>,nomor:25},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>2.     I was doing physically exercises all day yesterday. (Saya sedang melakukan latihan-latihan fisik sepanjang hari kemarin.)</Text>,nomor:26},
                {judul:<Text style={{marginLeft:5,marginTop:10, fontWeight:'bold'}}>B.      Untuk menunjukkan bahwa ada aksi berdurasi pendek (simple past tense) yang terjadi ketika suatu aksi berdurasi panjang (past continuous tense) sedang berlangsung. Subordinate conjunction while dan when dapat digunakan dengan formulasi simple past tense + while + past continuous tense dan past continuous tense + when + simple past tense.</Text>,nomor:27},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>       contoh Kalimat:</Text>,nomor:28},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>1.     She was sleeping when you called her. (Dia sedang tidur ketika kamu meneleponnya.)</Text>,nomor:29},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>2.     The door was knocked while I was reading a book. (Pintu diketuk ketika saya sedang membaca buku.)</Text>,nomor:30},
                {judul:<Text style={{marginLeft:5,marginTop:10, fontWeight:'bold'}}>C.     Untuk membahas sesuatu yang terjadi berulang-ulang. Interval kejadiannya acak namun sesungguhnya merupakan kebiasaan alami. Tense ini terkadang digunakan untuk mengkritik suatu aksi. </Text>,nomor:31},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>       Contoh Kalimat:</Text>,nomor:32},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>1.     I was repeatedly checking things. (Saya dulu berulang kali memeriksa sesuatu.)</Text>,nomor:33},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>2.     The girl was always yelling out loud. (Anak itu selalu menjerit keras-keras.)</Text>,nomor:34},
            ]
        }
    }
    render(){
        return(
            <View>
                <FlatList
                data = {this.state.yana}
                renderItem = {({item,index}) => (
                    <View>
                        {item.judul}
                    </View>
                )}
                keyExtractor = {(item) => item.nomor}
                />
            </View>
        )
    }
}

export default tenses6;