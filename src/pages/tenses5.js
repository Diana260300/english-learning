import React, { Component } from 'react';
import {View, StyleSheet, Text, FlatList} from 'react-native';

class tenses5 extends Component{
    constructor(props){
        super(props);
        this.state = {
            yana : [
                {judul:<Text style={{alignSelf:'center',fontWeight:'bold',fontSize:25,marginBottom:20}}>SIMPLE PAST TENSE </Text>,nomor:1},
                {judul:<Text style= {{marginLeft:5,}}>Simple past tense adalah kalimat tenses yang digunakan untuk menyatakan kejadian yang terjadi di masa lampau dan telah berakhir di masa lampau. Berbeda dengan past continous tense, yakni menyatakan kejadian yang terjadi di masa lampau, namun masih terjadi di masa sekarang.</Text>,nomor:2},
                {judul:<Text style= {{alignSelf:'center',fontSize:20,marginTop:10}}>Rumus Simple Past Tense</Text>,nomor:3},
                {judul:<Text style= {{marginLeft:5,marginTop:10}}>Untuk membentuk kalimat simple past tense, rumusnya adalah sebagai berikut</Text>,nomor:4},
                {judul:<Text style= {{marginLeft:5, marginTop:10, fontWeight:'bold'}}>1.    Kalimat Positive</Text>,nomor:5},
                {judul:<Text style= {{alignSelf:'center', fontWeight:'bold', marginTop:10}}>S + Verb 2 + O atau S + was/were + (adj/adv) + O</Text>,nomor:6},
                {judul:<Text style= {{marginLeft:5, marginTop:10}}>Dalam kalimat simple past tense, kata kerja yang digunakan merupakan bentuk kata kerja kedua. Terdapat dua jenis kata kerja, yakni regular verb dan irregular verb. Untuk regular verb, tambahkan -ed/-d dibelakang setelah kata kerja bentuk pertama. Sebagai contoh:</Text>,nomor:7},
                {judul:<Text style= {{marginLeft:5, marginTop:10}}>a.   Stay – stayed (Tinggal)</Text>,nomor:8},
                {judul:<Text style= {{marginLeft:5, marginTop:10}}>b.   Punch – Punched (Memukul)</Text>,nomor:9},
                {judul:<Text style= {{marginLeft:5, marginTop:10}}>c.   Play – Played (Bermain)</Text>,nomor:10},
                {judul:<Text style= {{marginLeft:5, marginTop:10}}>Untuk irregular verb , termasuk didalamnya to be, bentuk kata kerja keduanya sangat berbeda. Sebagai contoh:</Text>,nomor:11},
                {judul:<Text style= {{marginLeft:5, marginTop:10}}>a.   Awake – Awoke (Terbangun)</Text>,nomor:12},
                {judul:<Text style= {{marginLeft:5, marginTop:10}}>b.   Begin Drink – Drank (Minum) – Began (Memulai)</Text>,nomor:13},
                {judul:<Text style= {{marginLeft:5, marginTop:10}}>c.   Eat – ate (Makan)</Text>,nomor:14},
                {judul:<Text style= {{marginLeft:5, marginTop:10}}>d.   Run – ran (Berlari)</Text>,nomor:15},
                {judul:<Text style= {{marginLeft:5, marginTop:10}}>Namun, ada sebagian irregular verb yang memiliki bentuk kata kerja yang sama dengan bentuk kata kerja dasar. Sebagai contoh:</Text>,nomor:16},
                {judul:<Text style= {{marginLeft:5, marginTop:10}}>a.   Put – Put (Meletakkan)</Text>,nomor:17},
                {judul:<Text style= {{marginLeft:5, marginTop:10}}>b.   Spread – Spread (Menyebarkan)</Text>,nomor:18},
                {judul:<Text style= {{marginLeft:5, marginTop:10}}>c.   Set – Set (Mengatur)</Text>,nomor:19},
                {judul:<Text style= {{marginLeft:5, marginTop:10}}>d.   Cut – Cut (Memotong)</Text>,nomor:20},
                {judul:<Text style= {{marginLeft:5, marginTop:10}}> a.   Contoh-contoh kalimat simple past tense positive:</Text>,nomor:21},
                {judul:<Text style= {{marginLeft:5, marginTop:10}}>1.   Abdul went to Bali for holiday last Sunday (Minggu kemarin, Abdul pergi ke Bali untuk berlibur)</Text>,nomor:22},
                {judul:<Text style= {{marginLeft:5, marginTop:10}}>2.   She joined the English club class at school yesterday. (Kemarin, dia bergabung dalam klub Bahasa Inggris di sekolah).</Text>,nomor:23},
                {judul:<Text style= {{marginLeft:5, marginTop:10}}>3.   Last Monday, Joni got in an accident at the office (Senin lalu, Joni terkena musibah di kantor).</Text>,nomor:24},
                {judul:<Text style= {{marginLeft:5, marginTop:10}}>4.   They watched “Dilan”, the newest movie at the cinema yesterday. (Kemarin, mereka menonton film “Dilan”, film terbaru di bioskop)</Text>,nomor:25},
                {judul:<Text style= {{marginLeft:5, marginTop:10}}>5.   I was born in Surabaya. (Saya lahir di Surabaya)</Text>,nomor:26},
                {judul:<Text style= {{marginLeft:5, marginTop:10, fontWeight:'bold'}}>2.    Kalimat Negative</Text>,nomor:27},
                {judul:<Text style= {{marginLeft:5, marginTop:10}}>Untuk membentuk kalimat simple past tense negative, rumusnya adalah sebagai berikut</Text>,nomor:28},
                {judul:<Text style= {{alignSelf:'center', fontWeight:'bold', marginTop:10}}>S + did + not + Verb 1  atau  S + To Be (Was / Were) + not</Text>,nomor:29},
                {judul:<Text style= {{marginLeft:5, marginTop:10}}>b.   Contoh-contoh kalimat simple past tense negative </Text>,nomor:30},
                {judul:<Text style= {{marginLeft:5, marginTop:10}}>1.   I did not sleep well last night. (Aku tidak bisa tidur dengan nyenyak tadi malam).</Text>,nomor:31},
                {judul:<Text style= {{marginLeft:5, marginTop:10}}>2.   Rani did not come to the office yesterday. (Rani tidak datang ke kantor kemarin)</Text>,nomor:32},
                {judul:<Text style= {{marginLeft:5, marginTop:10}}>3.   Adi did not win the English debate competition last month. (Adi tidak memenangkan kompetisi debat berbahasa inggris bulan lalu)</Text>,nomor:33},
                {judul:<Text style= {{marginLeft:5, marginTop:10}}>4.   Arif was not the smartest students in the class. (Dulu Arif bukan murid yang paling pintar di kelas)</Text>,nomor:34},
                {judul:<Text style= {{marginLeft:5, marginTop:10}}>5.   John did not buy a car. (John tidak membeli sebuah mobil)</Text>,nomor:35},
                {judul:<Text style= {{marginLeft:5, marginTop:10, fontWeight:'bold'}}>3.    Kalimat Introgative </Text>,nomor:36},
                {judul:<Text style= {{marginLeft:5, marginTop:10}}>Untuk membentuk kalimat simple past tense interrogative, rumusnya adalah sebagai berikut:</Text>,nomor:37},
                {judul:<Text style= {{alignSelf:'center', fontWeight:'bold', marginTop:10}}>Did + S + Verb 1 atau Was / Were + S</Text>,nomor:38},
                {judul:<Text style= {{marginLeft:5, marginTop:10}}>c.   Contoh-contoh kalimat simple past tense introgative</Text>,nomor:39},
                {judul:<Text style= {{marginLeft:5, marginTop:10}}>1.   Did you see my bag on the table? (Apakah kamu melihat tasku di atas meja?)</Text>,nomor:40},
                {judul:<Text style= {{marginLeft:5, marginTop:10}}>2.   Was he so busy? (Apakah dia sangat sibuk?)</Text>,nomor:41},
                {judul:<Text style= {{marginLeft:5, marginTop:10}}>3.   Did they allow you to join their English club? (Apakah mereka mengizinkan kamu bergabung ke dalam klub Bahasa Inggris?)</Text>,nomor:42},
                {judul:<Text style= {{marginLeft:5, marginTop:10}}>4.   Was the movie so fantastic? Tell me (Apakah filmnya sangat berkesan? Ceritakan kepadaku.)</Text>,nomor:43},
                {judul:<Text style= {{marginLeft:5, marginTop:10}}>5.   Did he clean your room yesterday? (Apakah dia membersihkan ruanganmu kemarin?)</Text>,nomor:44},

            ]
        }
    }
    render(){
        return(
            <View>
                <FlatList
                data = {this.state.yana}
                renderItem = {({item,index}) => (
                    <View>
                        {item.judul}
                    </View>
                )}
                keyExtractor = {(item) => item.nomor}
                />
            </View>
        )
    }
}

export default tenses5;