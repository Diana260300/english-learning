import React, { Component } from 'react';
import {View, StyleSheet, Text, FlatList} from 'react-native';

class tenses11 extends Component{
    constructor(props){
        super(props);
        this.state = {
            yana : [
                {judul:<Text style={{alignSelf:'center',fontWeight:'bold',fontSize:25,marginBottom:20}}>FUTURE PERFECT TENSE</Text>,nomor:1},
                {judul:<Text style={{marginLeft:5}}>Future perfect tense adalah tenses Inggris yang berfungsi untuk menjelaskan suatu kejadian atau tindakan yang sudah selesai di masa depan. Saat kamu menggunakan tenses ini, kamu membayangkan dirimu di masa depan dan membicarakan kejadian atau tindakan yang akan selesai setelah sekarang. Lalu, apa perbedaannya dengan present perfect tense? Kalau present perfect tense membicarakan sebuah kejadian atau tindakan di masa lalu yang penting masa sekarang. Supaya kamu lebih mudah memahami perbedaannya, berikut contoh future perfect tense dan present perfect tense:</Text>,nomor:2},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>           Present Perfect Tense</Text>,nomor:3},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>           I’ve finished the homework.</Text>,nomor:4},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>           Future Perfect Tense</Text>,nomor:5},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>           I’ll have finished the homework by Sunday.</Text>,nomor:6},
                {judul:<Text style={{alignSelf:'center',fontSize:20,marginTop:10}}>Rumus Future Perfect Tense</Text>,nomor:7},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>Future perfect tense menggunakan past participle atau verb 3. Kamu perlu mempelajarinya agar lebih mudah menguasai bentuk future perfect.</Text>,nomor:8},
                {judul:<Text style={{marginLeft:5,marginTop:10, fontWeight:'bold'}}>1.      Kalimat Positive (+)</Text>,nomor:9},
                {judul:<Text style={{alignSelf:'center', fontWeight:'bold', marginTop:10}}>Subject + will + have + verb 3 (past participle) + object</Text>,nomor:10},
                {judul:<Text style={{marginLeft:5,marginTop:10, fontWeight:'bold'}}>2.      Kalimat Negative (-)</Text>,nomor:11},
                {judul:<Text style={{alignSelf:'center', fontWeight:'bold', marginTop:10}}>Subject + will + have + not + verb 3 (past participle) + object</Text>,nomor:12},
                {judul:<Text style={{marginLeft:5,marginTop:10, fontWeight:'bold'}}>3.      Kalimat Interrogative (+)</Text>,nomor:13},
                {judul:<Text style={{alignSelf:'center', fontWeight:'bold', marginTop:10}}>Will + subject + have + verb 3 (past participle) + object</Text>,nomor:14},
                {judul:<Text style={{marginLeft:5,marginTop:10, fontWeight:'bold'}}>4.      Kalimat Interrogative (-)</Text>,nomor:15},
                {judul:<Text style={{alignSelf:'center', fontWeight:'bold', marginTop:10}}>Will + not + subject + have + not + verb 3 (past participle) + object</Text>,nomor:16},
                {judul:<Text style={{alignSelf:'center',fontSize:20,marginTop:10}}>Contoh Future Perfect Tense</Text>,nomor:17},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>Berikut beberapa contoh kalimat future perfect tense:</Text>,nomor:18},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>1.     I won’t have finished it tomorrow.</Text>,nomor:19},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>        Saya tidak akan menyelesaikannya besok.</Text>,nomor:20},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>2.     Won’t they have finished it tomorrow?</Text>,nomor:21},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>       Bukankah mereka akan menyelesaikannya besok?</Text>,nomor:22},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>3.     Rudy will have arrived by lunchtime.</Text>,nomor:23},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>       Rudy akan sampai pada waktu makan siang.</Text>,nomor:24},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>4.     Will Rudy have arrived by lunchtime?</Text>,nomor:25},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>       Akankah Rudy sampai pada waktu makan siang?</Text>,nomor:26},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>5.     Let’s wait until 1 pm. The meal will have cooked by then</Text>,nomor:27},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>       Mari kita tunggu sampai jam 1 siang. Makanan sudah matang pada saat itu.</Text>,nomor:28},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>6.     You won’t have slept when I arrive, will you?</Text>,nomor:29},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>       Kamu tidak akan tidur ketika saya tiba, kan?</Text>,nomor:30},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>7.     Lizzie and Cacha will have eaten at midnight tonight.</Text>,nomor:31},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>       Lizzie dan Cacha akan makan pada tengah malam nanti.</Text>,nomor:32},
            ]
        }
    }
    render(){
        return(
            <View>
                <FlatList
                data = {this.state.yana}
                renderItem = {({item,index}) => (
                    <View>
                        {item.judul}
                    </View>
                )}
                keyExtractor = {(item) => item.nomor}
                />
            </View>
        )
    }
}

export default tenses11;