import React, { Component } from 'react';
import {View, StyleSheet, Text,FlatList} from 'react-native';



class phrases3 extends Component{
    constructor(props){
        super(props);
        this.state = {
            tugas : [
                {teks:<Text style={styles.modif1}>1.    I will be ready twenty four seven to help you : Aku akan siap setiap saat untuk membantumu</Text>,nomor:1},
                {teks:<Text style={styles.modif2}>2.    Take it easy  :  Santai saja</Text>,nomor:2},
                {teks:<Text style={styles.modif1}>3.    Would you please give me a hand to ...  :   Sudikah kamu membantuku untuk...</Text>,nomor:3},
                {teks:<Text style={styles.modif2}>4.    I know it like the back of my hand : Aku sangat mengenalnya</Text>,nomor:4},
                {teks:<Text style={styles.modif1}>5.    Iam sick and tired with his act :   Sya benci dengan prilakunya</Text>,nomor:5},
                {teks:<Text style={styles.modif2}>6.    Sleep on it        :        Tidak perlu buru-buru</Text>,nomor:6},
                {teks:<Text style={styles.modif1}>7.    Iam broke       :       Aku sedang tidak ada uang </Text>,nomor:7},
                {teks:<Text style={styles.modif2}>8.    It is just a puppy love     :   Itu cuma cinta monyet</Text>,nomor:8},
                {teks:<Text style={styles.modif1}>9.    I think we have something in common     :   Aku pikir kita memiliki persamaan</Text>,nomor:9},
                {teks:<Text style={styles.modif2}>10.   Iam out of the blue     :   Aku sudah melupakannya</Text>,nomor:10},
                {teks:<Text style={styles.modif1}>11.   I think you can win, Break a leg!   :   Aku pikir kamu bisa menang, semangat!</Text>,nomor:11},
                {teks:<Text style={styles.modif2}>12.   Iam sick to you     :   Aku sangat merindukanmu </Text>,nomor:12},
                {teks:<Text style={styles.modif1}>13.   Iam sick of you     :   Aku sangat membencimu </Text>,nomor:13},
                {teks:<Text style={styles.modif2}>14.   All the best for you    :   Semua yang terbaik untukmu</Text>,nomor:14},
                {teks:<Text style={styles.modif1}>15.   What's going on?    :   Apa yang terjadi?</Text>,nomor:15},
                {teks:<Text style={styles.modif2}>16.   Long time no see    :   Lama tak bertemu</Text>,nomor:16},
                {teks:<Text style={styles.modif1}>17.   Iam in good mood    :   Aku dalam suasana hati yang baik</Text>,nomor:17},
                {teks:<Text style={styles.modif2}>18.   It's Over   :   Ini sudah berakhir</Text>,nomor:18},
                {teks:<Text style={styles.modif1}>19.   Wish me luck    :   Doakan aku berhasil</Text>,nomor:19},
                {teks:<Text style={styles.modif2}>20.    As soon as possible     :   Secepat mungkin</Text>,nomor:20},
            ],
        }
    }
    render(){
        return(
            <View>
                <FlatList
                data = {this.state.tugas}
                renderItem = {({item,index}) => (
                    <View>
                        {item.teks}
                    </View>
                )}
                keyExtractor = {(item) => item.nomor}
                />
            </View>
        )
    }
};
const styles = StyleSheet.create({
    modif1:{
        fontSize:20, borderColor:'black', width:400, height:60, paddingHorizontal:10,
        borderRadius:5, marginTop:15, backgroundColor:'wheat', color:'black',
        paddingTop:10,marginHorizontal:10,
    },
    modif2:{
        fontSize:20, borderColor:'black', width:400, height:60, paddingHorizontal:10,
        borderRadius:5, marginTop:15, backgroundColor:'silver', color:'black',
        paddingTop:10,marginHorizontal:10,
    },
})
export default phrases3;