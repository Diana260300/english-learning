import React, { Component } from 'react';
import {View, StyleSheet, Text,FlatList} from 'react-native';



class vocabulary2 extends Component{
    constructor(props){
        super(props);
        this.state = {
            tugas : [
                {teks:<Text style={styles.modif1}>1.    Candle      :       Lilin  </Text>,nomor:1},
                {teks:<Text style={styles.modif2}>2.    Candy       :       Permen </Text>,nomor:2},
                {teks:<Text style={styles.modif1}>3.    Conversation    :       Percakapan </Text>,nomor:3},
                {teks:<Text style={styles.modif2}>4.    Challenge   :       Tantangan </Text>,nomor:4},
                {teks:<Text style={styles.modif1}>5.    Company     :       Perusahaan </Text>,nomor:5},
                {teks:<Text style={styles.modif2}>6.    Calculating     :       Menghitung </Text>,nomor:6},
                {teks:<Text style={styles.modif1}>7.    Careful     :       Cermat </Text>,nomor:7},
                {teks:<Text style={styles.modif2}>8.    Charming    :       Menawan </Text>,nomor:8},
                {teks:<Text style={styles.modif1}>9.    Cautious    :       Waspada </Text>,nomor:9},
                {teks:<Text style={styles.modif2}>10.   Cheap       :       Murah </Text>,nomor:10},
                {teks:<Text style={styles.modif1}>11.   Dark        :       Gelap</Text>,nomor:11},
                {teks:<Text style={styles.modif2}>12.   Deadly      :       Mematikan</Text>,nomor:12},
                {teks:<Text style={styles.modif1}>13.   Dangerous   :       Berbahaya</Text>,nomor:13},
                {teks:<Text style={styles.modif2}>14.   Deep        :       Dalam</Text>,nomor:14},
                {teks:<Text style={styles.modif1}>15.   Delayed     :       Terlambat</Text>,nomor:15},
                {teks:<Text style={styles.modif2}>16.   Day         :       Hari</Text>,nomor:16},
                {teks:<Text style={styles.modif1}>17.   Different   :       Berbeda</Text>,nomor:17},
                {teks:<Text style={styles.modif2}>18.   Daughter    :       Anak Perempuan</Text>,nomor:18},
                {teks:<Text style={styles.modif1}>19.   Difficult   :       Sulit</Text>,nomor:19},
                {teks:<Text style={styles.modif2}>20.   Disappear   :       Menghilang </Text>,nomor:20},
                {teks:<Text style={styles.modif1}>21.   Earth       :       Bumi </Text>,nomor:21},
                {teks:<Text style={styles.modif2}>22.   Education      :       Pendidikan</Text>,nomor:22},
                {teks:<Text style={styles.modif1}>23.   Enough         :       Cukup </Text>,nomor:23},
                {teks:<Text style={styles.modif2}>24.   Environment      :       Lingkungan</Text>,nomor:24},
                {teks:<Text style={styles.modif1}>25.   Evening       :       Malam Hari </Text>,nomor:25},
                {teks:<Text style={styles.modif2}>26.   Every      :       Setiap</Text>,nomor:26},
                {teks:<Text style={styles.modif1}>27.   Elegant      :       Anggun</Text>,nomor:27},
                {teks:<Text style={styles.modif2}>28.   Emotional       :       Emosional</Text>,nomor:28},
                {teks:<Text style={styles.modif1}>29.   Empty      :       Kosong</Text>,nomor:29},
                {teks:<Text style={styles.modif2}>30.   Energetic       :       Aktif</Text>,nomor:30},
            ],
        }
    }
    render(){
        return(
            <View>
                <FlatList
                data = {this.state.tugas}
                renderItem = {({item,index}) => (
                    <View>
                        {item.teks}
                    </View>
                )}
                keyExtractor = {(item) => item.nomor}
                />
            </View>
        )
    }
};
const styles = StyleSheet.create({
    modif1:{
        fontSize:20, borderColor:'black', width:400, height:50, paddingHorizontal:10,
        borderRadius:5, marginTop:15, backgroundColor:'papayawhip', color:'black',
        paddingTop:10,marginHorizontal:10,
    },
    modif2:{
        fontSize:20, borderColor:'black', width:400, height:50, paddingHorizontal:10,
        borderRadius:5, marginTop:15, backgroundColor:'gray', color:'black',
        paddingTop:10,marginHorizontal:10,
    },
})
export default vocabulary2;