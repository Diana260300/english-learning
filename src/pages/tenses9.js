import React, { Component } from 'react';
import {View, StyleSheet, Text, FlatList} from 'react-native';

class tenses9 extends Component{
    constructor(props){
        super(props);
        this.state = {
            yana : [
                {judul:<Text style={{alignSelf:'center',fontWeight:'bold',fontSize:25,marginBottom:20}}>SIMPLE FUTURE TENSE </Text>,nomor:1},
                {judul:<Text style={{marginLeft:5}}>Simple future tense adalah kalimat tenses yang digunakan untuk menunjukkan kejadian yang belum terjadi atau akan terjadi di masa depan dan berakhir di masa depan.</Text>,nomor:2},
                {judul:<Text style={{alignSelf:'center',fontSize:20,marginTop:10}}>Rumus Simple Future Tense</Text>,nomor:3},
                {judul:<Text style={{marginLeft:5,marginTop:10, fontWeight:'bold'}}>1.     Kalimat Positif </Text>,nomor:4},
                {judul:<Text style={{alignSelf:'center', fontWeight:'bold', marginTop:10}}>S + will/shall + Verb 1 + O Atau S + To Be (is, am, are) + Going To + Verb 1 + O</Text>,nomor:5},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>       Contoh Kalimat Simple Future Tense:</Text>,nomor:6},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>a.     They will take school examination next month. (Mereka akan mengikuti ujian sekolah bulan depan).</Text>,nomor:7},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>b.     I am going to take english class at Wall Street English next monday. (Saya akan mengikuti kelas bahasa Inggris di Wall Street English senin depan)</Text>,nomor:8},
                {judul:<Text style={{marginLeft:5,marginTop:10, fontWeight:'bold'}}>2.     Kalimat Negatif</Text>,nomor:9},
                {judul:<Text style={{alignSelf:'center', fontWeight:'bold', marginTop:10, textAlign:'justify',}}>    S + Will/shall + NOT + Verb 1 + O Atau S + To Be (is, am, are) + NOT + Going To + Verb 1 + O      </Text>,nomor:10},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>       Contoh Kalimat Simple Future Tense negatif:</Text>,nomor:11},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>a.     They are not going to go puncak because of the wheather. (Mereka tidak akan pergi ke puncak dikarenakan oleh cuaca)</Text>,nomor:12},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>b.     He will not join sport club at school. (Dia tidak akan bergabung klub olahraga di sekolah)</Text>,nomor:13},
                {judul:<Text style={{marginLeft:5,marginTop:10, fontWeight:'bold'}}>3.      Kalimat Introgatif </Text>,nomor:14},
                {judul:<Text style={{alignSelf:'center', fontWeight:'bold', marginTop:10}}>Will/shall + S + Verb 1 + O ? Atau To Be (is, am, are) + S + Going To + Verb 1 + O ?</Text>,nomor:15},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>       Contoh Kalimat Simple Future Tense introgatif:</Text>,nomor:16},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>a.     Will you come to my party tonight ? (Apakah kamu akan datang ke pesta saya, nanti malam ?)</Text>,nomor:17},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>b.     Are they going to go to Dufan tomorrow ? (Apakah mereka akan pergi ke Dufan besok ?)</Text>,nomor:18},
                {judul:<Text style={{alignSelf:'center',fontSize:20,marginTop:10}}>Fungsi Simple Future Tense</Text>,nomor:19},
                {judul:<Text style={{marginLeft:5,marginTop:10, fontWeight:'bold'}}>1.      Menyatakan suatu peristiwa atau kejadian yang akan terjadi pada waktu yang akan datang.</Text>,nomor:20},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>       Contoh Kalimat:</Text>,nomor:21},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>a.     I shall go to Manila tomorrow morning. (Aku akan pergi ke Manila besok pagi)</Text>,nomor:22},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>b.     They will arrive from Balikpapan in afew days. (Mereka akan tiba dari Balikpapan dalam waktu beberapa hari).</Text>,nomor:23},
                {judul:<Text style={{marginLeft:5,marginTop:10, fontWeight:'bold'}}>2.      Menyatakan suatu janji pada waktu yang akan datang.</Text>,nomor:24},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>       Contoh Kalimat:</Text>,nomor:25},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>a.     We shall meet the clients by noon. (Kami akan menemui klien-klien menjelang siang).</Text>,nomor:26},
                {judul:<Text style={{marginLeft:5,marginTop:10, fontWeight:'bold'}}>2.      Menyatakan suatu permintaan kepada orang lain.</Text>,nomor:27},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>       Contoh Kalimat:</Text>,nomor:28},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>a.     Will you help me get the book on the top shelf?. (Maukah kamu membantu saya untuk mengambil buku di tak atas?).</Text>,nomor:29},
                {judul:<Text style={{marginLeft:5,marginTop:10, fontWeight:'bold'}}>3.      Menyatakan suatu tawaran terhadap orang lain.</Text>,nomor:30},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>a.     Will you sit down by my side?. (Maukah kamu duduk di sampingku?).</Text>,nomor:31},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>Kata bantu shall atau will dapat pula digantikan dengan To Be + Going to dengan sedikit perbedaan dalam pengertiannya. Kita gunakan shall atau will apabila sesuatu sudah merupakan hal yang pasti akan kita kerjakan di waktu mendatang (certainty) atau janji (promise), Namun bila masih merupakan suatu niat (intention) maka kita gunakan to be + going to.</Text>,nomor:32},
            ]
        }
    }
    render(){
        return(
            <View>
                <FlatList
                data = {this.state.yana}
                renderItem = {({item,index}) => (
                    <View>
                        {item.judul}
                    </View>
                )}
                keyExtractor = {(item) => item.nomor}
                />
            </View>
        )
    }
}

export default tenses9;