import React, { Component } from 'react';
import {View, StyleSheet, Text,FlatList} from 'react-native';



class vocabulary4 extends Component{
    constructor(props){
        super(props);
        this.state = {
            tugas : [
                {teks:<Text style={styles.modif1}>1.    Ignore      :       Mengabaikan</Text>,nomor:1},
                {teks:<Text style={styles.modif2}>2.    Important       :       Penting </Text>,nomor:2},
                {teks:<Text style={styles.modif1}>3.    Impossible      :       Mustahil</Text>,nomor:3},
                {teks:<Text style={styles.modif2}>4.    Interesting     :       Menarik</Text>,nomor:4},
                {teks:<Text style={styles.modif1}>5.    Introduce       :       Memperkenalkan</Text>,nomor:5},
                {teks:<Text style={styles.modif2}>6.    Invite      :       Mengundang</Text>,nomor:6},
                {teks:<Text style={styles.modif1}>7.    Insult      :       Menghina</Text>,nomor:7},
                {teks:<Text style={styles.modif2}>8.    Illegal     :       Liar</Text>,nomor:8},
                {teks:<Text style={styles.modif1}>9.    Immediate       :       Segera</Text>,nomor:9},
                {teks:<Text style={styles.modif2}>10.   Ice Cream       :       Es Krim</Text>,nomor:10},
                {teks:<Text style={styles.modif1}>11.   Join        :       Bergabung </Text>,nomor:11},
                {teks:<Text style={styles.modif2}>12.   Jealous     :       Cemburu</Text>,nomor:12},
                {teks:<Text style={styles.modif1}>13.   Journey     :       Perjalanan</Text>,nomor:13},
                {teks:<Text style={styles.modif2}>14.   Jaded       :       Letih</Text>,nomor:14},
                {teks:<Text style={styles.modif1}>15.   Jolly       :       Riang </Text>,nomor:15},
                {teks:<Text style={styles.modif2}>16.   Jittery     :       Gelisah</Text>,nomor:16},
                {teks:<Text style={styles.modif1}>17.   Jobless     :       Pengangguran</Text>,nomor:17},
                {teks:<Text style={styles.modif2}>18.   Juicy       :       Berair </Text>,nomor:18},
                {teks:<Text style={styles.modif1}>19.   Joke        :       Bercanda</Text>,nomor:19},
                {teks:<Text style={styles.modif2}>20.   Jump        :       Melompat</Text>,nomor:20},
                {teks:<Text style={styles.modif1}>21.   Kick        :       Menendang</Text>,nomor:21},
                {teks:<Text style={styles.modif2}>22.   Kiss        :       Mencium</Text>,nomor:22},
                {teks:<Text style={styles.modif1}>23.   know        :       Tahu,Mengetahui</Text>,nomor:23},
                {teks:<Text style={styles.modif2}>24.   Kill        :       Membunuh</Text>,nomor:24},
                {teks:<Text style={styles.modif1}>25.   Knock       :       Mengetuk</Text>,nomor:25},
                {teks:<Text style={styles.modif2}>26.   Knowledge       :       Pengetahuan</Text>,nomor:26},
                {teks:<Text style={styles.modif1}>27.   Keep        :           Menjaga</Text>,nomor:27},
                {teks:<Text style={styles.modif2}>28.   Kneel       :       Berlutut</Text>,nomor:28},
                {teks:<Text style={styles.modif1}>29.   Knit        :       Merajut</Text>,nomor:29},
                {teks:<Text style={styles.modif2}>30.   Kidnap      :       Menculik</Text>,nomor:30},
            ],
        }
    }
    render(){
        return(
            <View>
                <FlatList
                data = {this.state.tugas}
                renderItem = {({item,index}) => (
                    <View>
                        {item.teks}
                    </View>
                )}
                keyExtractor = {(item) => item.nomor}
                />
            </View>
        )
    }
};
const styles = StyleSheet.create({
    modif1:{
        fontSize:20, borderColor:'black', width:400, height:50, paddingHorizontal:10,
        borderRadius:5, marginTop:15, backgroundColor:'papayawhip', color:'black',
        paddingTop:10,marginHorizontal:10,
    },
    modif2:{
        fontSize:20, borderColor:'black', width:400, height:50, paddingHorizontal:10,
        borderRadius:5, marginTop:15, backgroundColor:'gray', color:'black',
        paddingTop:10,marginHorizontal:10,
    },
})
export default vocabulary4;