import React, { Component } from 'react';
import {View, StyleSheet, Text, FlatList} from 'react-native';

class tenses12 extends Component{
    constructor(props){
        super(props);
        this.state = {
            yana : [
                {judul:<Text style={{alignSelf:'center',fontWeight:'bold',fontSize:25,marginBottom:20}}>FUTURE PERFECT CONTINUOUS TENSE</Text>,nomor:1},
                {judul:<Text style={{marginLeft:5}}>Future perfect continuous tense adalah bentuk kalimat yang digunakan untuk mengekspresikan tindakan sedang akan berlangsung atau berkelanjutan yang akan dimulai pada waktu tertentu atau di masa depan dan akan berlanjut sampai suatu saat nanti. Akan selalu ada referensi waktu lanjutan seperti : for ten months, for 2 years, since Monday, since June. Jika tidak ada referensi waktu, maka itu bukanlah future perfect continuous tense, tanpa referensi waktu yang berkelanjutan, kalimat seperti itu adalah future continuous tense.</Text>,nomor:2},
                {judul:<Text style={{marginLeft:5, marginTop:10}}>Referensi waktu yang berkelanjutan bisa membedakan antara future continuous tense dan future perfect continuous tense.</Text>,nomor:3},
                {judul:<Text style={{alignSelf:'center',fontSize:20,marginTop:10}}>Rumus future perfect continuous tense</Text>,nomor:4},
                {judul:<Text style={{marginLeft:5, marginTop:10, fontWeight:'bold'}}>1.     Rumus Bentuk Kalimat Positif</Text>,nomor:5},
                {judul:<Text style={{alignSelf:'center', fontWeight:'bold', marginTop:10}}>S + Will/Shall + Have + Been + V1-ing + O + Since/For + Ket. Waktu</Text>,nomor:6},
                {judul:<Text style={{marginLeft:5, marginTop:10}}>      Contoh:</Text>,nomor:7},
                {judul:<Text style={{marginLeft:5, marginTop:10}}>a.    I shall have been sitting under the tree since 10 A.M.</Text>,nomor:8},
                {judul:<Text style={{marginLeft:5, marginTop:10, fontWeight:'bold'}}>2.     Rumus Bentuk Kalimat Negatif</Text>,nomor:9},
                {judul:<Text style={{alignSelf:'center', fontWeight:'bold', marginTop:10}}>S + Will/Shall + not + Have + Been + V1-ing + O + Since/For + Ket. Waktu</Text>,nomor:10},
                {judul:<Text style={{marginLeft:5, marginTop:10}}>      Contoh:</Text>,nomor:11},
                {judul:<Text style={{marginLeft:5, marginTop:10}}>a.        She will have been swimming with her boyfriend for 3 hours.</Text>,nomor:12},
                {judul:<Text style={{marginLeft:5, marginTop:10, fontWeight:'bold'}}>3.     Rumus Bentuk Kalimat Interogatif</Text>,nomor:13},
                {judul:<Text style={{alignSelf:'center', fontWeight:'bold', marginTop:10}}>Will/Shall + S + Have + Been + V1-ing + O + Since/For + Ket. Waktu + ?</Text>,nomor:14},
                {judul:<Text style={{marginLeft:5, marginTop:10}}>      Contoh:</Text>,nomor:15},
                {judul:<Text style={{marginLeft:5, marginTop:10}}>a.    will she have been swimming with her boyfriend for 3 hours?</Text>,nomor:16},
                {judul:<Text style={{alignSelf:'center',fontSize:20,marginTop:10}}>Fungsi dan Ciri-ciri future perfect continuous tense </Text>,nomor:17},
                {judul:<Text style={{marginLeft:5, marginTop:10}}>      Fungsi future perfect continuous tense adalah untuk menyatakan tindakan yang sedang akan berlangsung dan terus terjadi sampai beberapa saat di masa depan dengan rumus Will + Have + Been + present participle (Verb-1 + ing) O + Since/For + Keterangan Waktu.</Text>,nomor:18},
                {judul:<Text style={{marginLeft:5, marginTop:10}}>      Ciri-ciri future perfect continuous tense adalah predikat kalimatnya sering dipakaii shall atau will dengan keterangan waktu “at this time tomorrow“, “for” atau “by the end of this week“.</Text>,nomor:19},
        
            ]
        }
    }
    render(){
        return(
            <View>
                <FlatList
                data = {this.state.yana}
                renderItem = {({item,index}) => (
                    <View>
                        {item.judul}
                    </View>
                )}
                keyExtractor = {(item) => item.nomor}
                />
            </View>
        )
    }
}

export default tenses12;