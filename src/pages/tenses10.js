import React, { Component } from 'react';
import {View, StyleSheet, Text, FlatList} from 'react-native';

class tenses10 extends Component{
    constructor(props){
        super(props);
        this.state = {
            yana : [
                {judul:<Text style={{alignSelf:'center',fontWeight:'bold',fontSize:25,marginBottom:20}}>FUTURE CONTINUOUS TENSE</Text>,nomor:1},
                {judul:<Text style={{marginLeft:5}}>Future continuous tense adalah suatu bentuk tense dalam bahasa Inggris yang biasanya disebut juga dengan future progressive tense. Penggunaaan tense ini menunjukkan bahwa suatu kejadian akan terjadi di masa depan dan terus berlanjut untuk beberapa waktu. Hal yang penting untuk diingat adalah bahwa suatu kejadian yang digambarkan dengan tense ini memiliki durasi waktu.</Text>,nomor:2},
                {judul:<Text style={{alignSelf:'center',fontSize:20,marginTop:10}}>Rumus Future Continuous Tense</Text>,nomor:3},
                {judul:<Text style={{marginLeft:5,marginTop:10, fontWeight:'bold'}}>1.     Rumus kalimat positif</Text>,nomor:4},
                {judul:<Text style={{alignSelf:'center', fontWeight:'bold', marginTop:10}}>subject + will/shall + be +verb-ing.</Text>,nomor:5},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>       Contoh kalimat:</Text>,nomor:6},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>a.     I will be staying with my sister for a week.</Text>,nomor:7},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>b.     I will be eating at 7 o’clock tonight.</Text>,nomor:8},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>c.     He will be coming to my birthday party tomorrow.</Text>,nomor:9},
                {judul:<Text style={{marginLeft:5,marginTop:10, fontWeight:'bold'}}>2.      Rumus kalimat negatif</Text>,nomor:10},
                {judul:<Text style={{alignSelf:'center', fontWeight:'bold', marginTop:10}}>subject + will/shall + not + be + verb ing + complement. </Text>,nomor:11},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>       Contoh Kalimat:</Text>,nomor:12},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>a.     I will not be staying with my sister for a week.</Text>,nomor:13},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>b.     Erika will not be competing against Mikaela in the race when the race starts.</Text>,nomor:14},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>c.     She will not be coming to your farewell party next week.</Text>,nomor:15},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>Will not dan shall not dapat disingkat dalam kalimat Bahasa Inggris menjadi won’t dan shan’t. Namun, penggunaan singkatan shan’t tidak lazim digunakan dalam perbincangan sehari-hari. Biasanya dipakai hanya dalam bentuk tulisan saja.</Text>,nomor:16},
                {judul:<Text style={{marginLeft:5,marginTop:10, fontWeight:'bold'}}>3.      Rumus kalimat introgatif</Text>,nomor:17},
                {judul:<Text style={{alignSelf:'center', fontWeight:'bold', marginTop:10}}>will /shall+ subject + be + verb-ing + complement ?</Text>,nomor:18},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>       Contoh Kalimat :</Text>,nomor:19},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>a.     Will Sunny and Rain be watching Mikaela race this Sunday evening?</Text>,nomor:20},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>b.     Will you be coming to Susi’s farewell party?</Text>,nomor:21},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>c.     Will James be exercising at the gym?</Text>,nomor:22},
                {judul:<Text style={{alignSelf:'center',fontSize:20,marginTop:10}}>Penggunaan Keterangan Waktu</Text>,nomor:23},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>Dalam menggunakan future continuous tense, ada keterangan waktu yang perlu ditulis untuk menjelaskan masa depan. Maka dari itu, kamu bisa menggunakan keterangan waktu di bawah ini sebagai referensi.</Text>,nomor:24},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>1.     Tomorrow night: Besok malam</Text>,nomor:25},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>2.     Next year on April: Tahun depan pada bulan April</Text>,nomor:26},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>3.     Tonight at seven o’clock: Nanti malam jam 07.00</Text>,nomor:27},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>4.     At this time tomorrow: Pada waktu ini besok</Text>,nomor:28},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>5.     At six o’clock tomorrow morning: Pada jam 6 besok pagi</Text>,nomor:29},
                {judul:<Text style={{marginLeft:5,marginTop:10, alignSelf:'center',fontSize:20,marginTop:10}}>Fungsi Future Continuous Tense</Text>,nomor:30},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>       Menyatakan sebuah kegiatan yang akan atau sedang dilakukan pada suatu waktu di masa yang akan datang atau di masa depan dengan keterangan waktu yang spesifik. Contoh: Henry will call you at 8 o’clock tomorrow morning.</Text>,nomor:31},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>       Menunjukkan kejadian mana yang sedang berlangsung di masa depan ketika ada kejadian lain yang juga terjadi di masa depan. Contoh: I will be playing games when you come tonight</Text>,nomor:32},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>       Mengajukan sebuah pertanyaan dengan sopan dan formal yang bertujuan untuk menyampaikan niat seseorang. Contoh: Will you be coming home?</Text>,nomor:33},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>Dapat disimpulkan bahwa penggunaan future continuous tense sama dengan past continuous. Pembedanya adalah bahwa kejadiannya terjadi di masa depan sehingga kita harus menggunakan keterangan waktu masa depan atau kalimat kedua dengan simple present tense.</Text>,nomor:34},
            ]
        }
    }
    render(){
        return(
            <View>
                <FlatList
                data = {this.state.yana}
                renderItem = {({item,index}) => (
                    <View>
                        {item.judul}
                    </View>
                )}
                keyExtractor = {(item) => item.nomor}
                />
            </View>
        )
    }
}

export default tenses10;