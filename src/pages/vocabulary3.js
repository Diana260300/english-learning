import React, { Component } from 'react';
import {View, StyleSheet, Text,FlatList} from 'react-native';



class vocabulary3 extends Component{
    constructor(props){
        super(props);
        this.state = {
            tugas : [
                {teks:<Text style={styles.modif1}>1.    Fabulous      :     Menakjubkan  </Text>,nomor:1},
                {teks:<Text style={styles.modif2}>2.    Faded       :       Luntur </Text>,nomor:2},
                {teks:<Text style={styles.modif1}>3.    Failinh       :        Kegagalan</Text>,nomor:3},
                {teks:<Text style={styles.modif2}>4.    Faint       :       Lemah </Text>,nomor:4},
                {teks:<Text style={styles.modif1}>5.    False      :        Salah</Text>,nomor:5},
                {teks:<Text style={styles.modif2}>6.    Fancy        :       Mewah </Text>,nomor:6},
                {teks:<Text style={styles.modif1}>7.    Far        :         Jauh</Text>,nomor:7},
                {teks:<Text style={styles.modif2}>8.    Fast        :       Cepat </Text>,nomor:8},
                {teks:<Text style={styles.modif1}>9.    Female      :       Wanita </Text>,nomor:9},
                {teks:<Text style={styles.modif2}>10.   Fertile         :       Subur </Text>,nomor:10},
                {teks:<Text style={styles.modif1}>11.   General     :       Umum</Text>,nomor:11},
                {teks:<Text style={styles.modif2}>12.   Ghastly     :       Mengerikan</Text>,nomor:12},
                {teks:<Text style={styles.modif1}>13.   Giant       :       Raksasa</Text>,nomor:13},
                {teks:<Text style={styles.modif2}>14.   Giddy       :       Pusing</Text>,nomor:14},
                {teks:<Text style={styles.modif1}>15.   Good        :       Baik </Text>,nomor:15},
                {teks:<Text style={styles.modif2}>16.   Gray        :       Abu-Abu</Text>,nomor:16},
                {teks:<Text style={styles.modif1}>17.   Garden      :       Kebun</Text>,nomor:17},
                {teks:<Text style={styles.modif2}>18.   Give        :       Memberi</Text>,nomor:18},
                {teks:<Text style={styles.modif1}>19.   Get         :       Mendapatkan</Text>,nomor:19},
                {teks:<Text style={styles.modif2}>20.   Garden      :       Tingkat</Text>,nomor:20},
                {teks:<Text style={styles.modif1}>21.   Hair        :       Rambut</Text>,nomor:21},
                {teks:<Text style={styles.modif2}>22.   Habit       :       Kebiasaan</Text>,nomor:22},
                {teks:<Text style={styles.modif1}>23.   Have        :       Memiliki </Text>,nomor:23},
                {teks:<Text style={styles.modif2}>24.   Health      :       Kesehatan</Text>,nomor:24},
                {teks:<Text style={styles.modif1}>25.   Heart       :       Hati </Text>,nomor:25},
                {teks:<Text style={styles.modif2}>26.   Heaven      :       Surga</Text>,nomor:26},
                {teks:<Text style={styles.modif1}>27.   Hell        :       Neraka</Text>,nomor:27},
                {teks:<Text style={styles.modif2}>28.   Honey       :       Madu</Text>,nomor:28},
                {teks:<Text style={styles.modif1}>29.   Hospital        :       Rumah Sakit</Text>,nomor:29},
                {teks:<Text style={styles.modif2}>30.   Honour      :       Kehormatan</Text>,nomor:30},
            ],
        }
    }
    render(){
        return(
            <View>
                <FlatList
                data = {this.state.tugas}
                renderItem = {({item,index}) => (
                    <View>
                        {item.teks}
                    </View>
                )}
                keyExtractor = {(item) => item.nomor}
                />
            </View>
        )
    }
};
const styles = StyleSheet.create({
    modif1:{
        fontSize:20, borderColor:'black', width:400, height:50, paddingHorizontal:10,
        borderRadius:5, marginTop:15, backgroundColor:'papayawhip', color:'black',
        paddingTop:10,marginHorizontal:10,
    },
    modif2:{
        fontSize:20, borderColor:'black', width:400, height:50, paddingHorizontal:10,
        borderRadius:5, marginTop:15, backgroundColor:'gray', color:'black',
        paddingTop:10,marginHorizontal:10,
    },
})
export default vocabulary3;