import React, { Component } from 'react';
import {View, StyleSheet, Text,FlatList} from 'react-native';



class phrases5 extends Component{
    constructor(props){
        super(props);
        this.state = {
            tugas : [
                {teks:<Text style={styles.modif1}>1.    You don't seem to remember   :   Kelihatannya kamu tidak ingat</Text>,nomor:1},
                {teks:<Text style={styles.modif2}>2.    We feel so bad about it :   kami merasa sangat menyesal karena itu</Text>,nomor:2},
                {teks:<Text style={styles.modif1}>3.    I'm trying to entertain her, that's all :   Aku mencoba menghiburnya, itu saja</Text>,nomor:3},
                {teks:<Text style={styles.modif2}>4.    It's a thrilling moment     :   ini saat-saat yang menegangkan</Text>,nomor:4},
                {teks:<Text style={styles.modif1}>5.    this will never happen again    :   Ini tak akan terjadi lagi </Text>,nomor:5},
                {teks:<Text style={styles.modif2}>6.    He needs to empty his mind      :   Dia harus menenangkan pikirannya </Text>,nomor:6},
                {teks:<Text style={styles.modif1}>7.    How can you be like that?      :   Kenapa kau bisa seperti ini?</Text>,nomor:7},
                {teks:<Text style={styles.modif2}>8.    I Think You are great   :   Aku pikir kamu hebat</Text>,nomor:8},
                {teks:<Text style={styles.modif1}>9.    I'm sorry, I went over the line     :   Maaf, aku sudah kelewatan</Text>,nomor:9},
                {teks:<Text style={styles.modif2}>10.   Wait for a while    :   Tunggu sebentar</Text>,nomor:10},
                {teks:<Text style={styles.modif1}>11.   Don't tell anyone   :   Jangan bilang siapa-siapa</Text>,nomor:11},
                {teks:<Text style={styles.modif2}>12.   I'm sorry for being to late     :   Maaf aku datang terlambat</Text>,nomor:12},
                {teks:<Text style={styles.modif1}>13.   You should've told me   :   Kau harusnya bilang padaku </Text>,nomor:13},
                {teks:<Text style={styles.modif2}>14.   Do gardening        :       Berkebun</Text>,nomor:14},
                {teks:<Text style={styles.modif1}>15.   Plan the flower     :       Menanam bunga</Text>,nomor:15},
                {teks:<Text style={styles.modif2}>16.   Skimp the money     :       Menghemat uang</Text>,nomor:16},
                {teks:<Text style={styles.modif1}>17.   Prepare the lunch       :       Menyiapkan makan siang </Text>,nomor:17},
                {teks:<Text style={styles.modif2}>18.   Thank you for your attention    :   Terimakasih untuk perhatian anda</Text>,nomor:18},
                {teks:<Text style={styles.modif1}>19.   I have nothing to say       :   Aku tidak bisa berkata apa-apa</Text>,nomor:19},
                {teks:<Text style={styles.modif2}>20.   Don't go anywhere, I'll be back     :   Jangan kemana-mana, aku akan kembali</Text>,nomor:20},
            ],
        }
    }
    render(){
        return(
            <View>
                <FlatList
                data = {this.state.tugas}
                renderItem = {({item,index}) => (
                    <View>
                        {item.teks}
                    </View>
                )}
                keyExtractor = {(item) => item.nomor}
                />
            </View>
        )
    }
};
const styles = StyleSheet.create({
    modif1:{
        fontSize:20, borderColor:'black', width:400, height:60, paddingHorizontal:10,
        borderRadius:5, marginTop:15, backgroundColor:'wheat', color:'black',
        paddingTop:10,marginHorizontal:10,
    },
    modif2:{
        fontSize:20, borderColor:'black', width:400, height:60, paddingHorizontal:10,
        borderRadius:5, marginTop:15, backgroundColor:'silver', color:'black',
        paddingTop:10,marginHorizontal:10,
    },
})
export default phrases5;