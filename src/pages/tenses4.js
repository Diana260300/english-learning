import React, { Component } from 'react';
import {View, StyleSheet, Text,FlatList} from 'react-native';

class tenses4 extends Component{
    constructor(props){
        super(props);
        this.state = {
            yana : [
                {judul:<Text style={{alignSelf:'center',fontWeight:'bold',fontSize:25,marginBottom:20}}>PRESENT PERFECT CONTINUOUS TENSE</Text>,nomor:1},
                {judul:<Text style= {{marginLeft:5,}}>Present perfect continuous tense adalah suatu bentuk kata kerja yang digunakan untuk menyatakan aksi yang telah selesai pada suatu titik di masa lalu atau aksi telah dimulai di masa lalu dan terus berlanjut sampai sekarang. Aksi pada tense ini biasanya berdurasi waktu tertentu dan ada relevansinya dengan kondisi sekarang.</Text>,nomor:2},
                {judul:<Text style= {{alignSelf:'center',fontSize:20,marginTop:10}}>Rumus Present Perfect Continuous</Text>,nomor:3},
                {judul:<Text style= {{marginLeft:5,marginTop:10}}>Agar lebih mudah memahaminya, kamu hanya perlu melihat dari nama tense ini untuk menggunakan kata kerja yang tepat.</Text>,nomor:4},
                {judul:<Text style= {{marginLeft:5,marginTop:10}}>Present perfect continuous tense dibentuk dengan auxiliary verb have atau has, been dan present participle (-ing). Have digunakan untuk I, you, dan plural subject seperti: plural pronoun (seperti: they, we), plural noun (seperti: boys, men), dan compound subject dengan kata hubung “and” (seperti: you and I, Tom and Jack). Sedangkan has untuk singular subject seperti: third person singular pronoun (seperti: he, she, it) dan singular noun (seperti: Tom, man). Secara umum present perfect continuous tense hanya terjadi pada aksi berupa dynamic verb, tidak stative verb karena umumnya hanya dynamic verb yang memiliki bentuk continuous. Rumus tense ini dibagi menjadi 3 tergantung dari jenis kalimatnya, antara lain:</Text>,nomor:5},
                {judul:<Text style= {{marginLeft:5,marginTop:10, fontWeight:'bold'}}>1.     Kalimat positif</Text>,nomor:6},
                {judul:<Text style= {{alignSelf:'center', fontWeight:'bold', marginTop:10}}>S + have/has + been + -ing/present participle</Text>,nomor:7},
                {judul:<Text style= {{marginLeft:5,marginTop:10}}>  Contoh kalimatnya sebagai berikut:</Text>,nomor:8},
                {judul:<Text style= {{marginLeft:5,marginTop:10}}>a.    She has been driving. (Dia telah menyetir)</Text>,nomor:9},
                {judul:<Text style= {{marginLeft:5,marginTop:10}}>b.    The toddlers have been sleeping. (Anak-anak sudah tidur)</Text>,nomor:10},
                {judul:<Text style= {{marginLeft:5,marginTop:10, fontWeight:'bold'}}>2.     Kalimat negatif</Text>,nomor:11},
                {judul:<Text style= {{alignSelf:'center', fontWeight:'bold', marginTop:10}}>S + have/has + not + been + -ing/present participle.</Text>,nomor:12},
                {judul:<Text style= {{marginLeft:5,marginTop:10}}>  Adapun contoh kalimatnya adalah:</Text>,nomor:13},
                {judul:<Text style= {{marginLeft:5,marginTop:10}}>a.    She has not been driving. (Dia belum  menyetir.)</Text>,nomor:14},
                {judul:<Text style= {{marginLeft:5,marginTop:10}}>b.    The toddlers haven’t been sleeping. (Anak-anak belum tidur.)</Text>,nomor:15},
                {judul:<Text style= {{marginLeft:5,marginTop:10, fontWeight:'bold'}}>3.     Kalimat introgatif</Text>,nomor:16},
                {judul:<Text style= {{alignSelf:'center', fontWeight:'bold', marginTop:10}}>have/has + S + been + -ing/present participle.</Text>,nomor:17},
                {judul:<Text style= {{marginLeft:5,marginTop:10}}>  Contoh kalimatnya sebagai berikut:</Text>,nomor:18},
                {judul:<Text style= {{marginLeft:5,marginTop:10}}>a.    Has she been driving? (Apakah ia sudah menyetir?)</Text>,nomor:19},
                {judul:<Text style= {{marginLeft:5,marginTop:10}}>b.    Have the toddlers been sleeping? (Apakah anak-anak sudah tidur?)</Text>,nomor:20},
                {judul:<Text style= {{marginLeft:40,marginTop:10,fontSize:20}}>Penggunaan dan Contoh Kalimat Present Perfect Continuous</Text>,nomor:21},
                {judul:<Text style= {{marginLeft:5,marginTop:10, fontWeight:'bold'}}>a. Menunjukkan aksi yang telah selesai</Text>,nomor:22},
                {judul:<Text style= {{marginLeft:5,marginTop:10}}>  Present perfect continuous tense untuk menunjukkan aksi yang telah selesai pada suatu titik dimasa lampau diikuti relevansinya dengan kondisi saat ini (present). Contoh:</Text>,nomor:23},
                {judul:<Text style= {{marginLeft:5,marginTop:10}}>1. The construction laborers are thirsty since they have been removing the scaffoldings. (Pekerja konstruksi haus karena mereka telah memindahkan perancah.)</Text>,nomor:24},
                {judul:<Text style= {{marginLeft:5,marginTop:10}}>2.  have been working with my computer all day, and now I want some delicious food. (Saya telah bekerja dengan komputer sepanjang hari, dan sekarang saya ingin beberapa makanan lezat.)</Text>,nomor:25},
                {judul:<Text style= {{marginLeft:5,marginTop:10, fontWeight:'bold'}}>b. Menunjukkan aksi yang baru saja selesai</Text>,nomor:26},
                {judul:<Text style= {{marginLeft:5,marginTop:10}}>  Selain untuk menunjukkan sebuah aksi yang masih berlangsung sampai sekarang, tense ini juga bisa digunakan ketika kamu ingin menunjukkan sebuah aksi atau kondisi yang baru saja selesai kamu lakukan. Adverb “just” digunakan untuk menunjuk kondisi ini. Contoh :</Text>,nomor:27},
                {judul:<Text style= {{marginLeft:5,marginTop:10}}>1. It has just been snowing in Japan. (Baru saja turun salju di Jepang.)</Text>,nomor:28},
                {judul:<Text style= {{marginLeft:5,marginTop:10}}>2. Please take off your shoes because I just have been sweeping the floor. (Tolong copot sepatumu karena aku baru saja menyapu lantainya.)</Text>,nomor:29},
                {judul:<Text style= {{marginLeft:5,marginTop:10, fontWeight:'bold'}}>c. Menunjuk aksi yang telah dimulai pada masa lampau</Text>,nomor:30},
                {judul:<Text style= {{marginLeft:5,marginTop:10}}>  Apakah sebuah aksi masih berlangsung sampai sekarang walaupun dimulai di masa lampau? Kamu tetap bisa menggunakan tense ini. Contoh: </Text>,nomor:31},
                {judul:<Text style= {{marginLeft:5,marginTop:10}}>1. I’ve been driving a car through the rain for an hour. (Saya sudah mengendarai mobil menembus hujan selama satu jam.)</Text>,nomor:32},
                {judul:<Text style= {{marginLeft:5,marginTop:10}}>2. The passengers have been waiting for the next flight since this morning. (Para penumpang telah menunggu penerbangan berikutnya sejak pagi ini.)</Text>,nomor:33},

            ]
        }
    }
    render(){
        return(
            <View>
                <FlatList
                data = {this.state.yana}
                renderItem = {({item,index}) => (
                    <View>
                        {item.judul}
                    </View>
                )}
                keyExtractor = {(item) => item.nomor}
                />
            </View>
        )
    }
}

export default tenses4;