import React, { Component } from 'react';
import {View, StyleSheet, Text,TouchableOpacity,Image, FlatList, Dimensions} from 'react-native';

const numColumns= 2;
const WIDTH = Dimensions.get('window').width;

class vocabulary extends Component{
    constructor(props){
        super(props);
        this.state = {
            yana : [
                {gambar:<TouchableOpacity onPress={() => this.props.navigation.navigate('Simple Present Tense')}><Image source={require('../assets/T1.png')} style={styles.foto}/></TouchableOpacity>, nomor:1},
                {gambar:<TouchableOpacity onPress={() => this.props.navigation.navigate('Present Continuous Tense')}><Image source={require('../assets/T2.png')} style={styles.foto}/></TouchableOpacity>, nomor:2},
                {gambar:<TouchableOpacity onPress={() => this.props.navigation.navigate('Present Perfect Tense')}><Image source={require('../assets/T3.png')} style={styles.foto}/></TouchableOpacity>, nomor:3},
                {gambar:<TouchableOpacity onPress={() => this.props.navigation.navigate('Present Perfect Continuous Tense')}><Image source={require('../assets/T4.png')} style={styles.foto}/></TouchableOpacity>, nomor:4},
                {gambar:<TouchableOpacity onPress={() => this.props.navigation.navigate('Simple Past Tense')}><Image source={require('../assets/T5.png')} style={styles.foto}/></TouchableOpacity>, nomor:5},
                {gambar:<TouchableOpacity onPress={() => this.props.navigation.navigate('Past Continuous Tense')}><Image source={require('../assets/T6.png')} style={styles.foto}/></TouchableOpacity>, nomor:6},
                {gambar:<TouchableOpacity onPress={() => this.props.navigation.navigate('Past Perfect Tense')}><Image source={require('../assets/T7.png')} style={styles.foto}/></TouchableOpacity>, nomor:7},
                {gambar:<TouchableOpacity onPress={() => this.props.navigation.navigate('Past Perfect Continuous Tense')}><Image source={require('../assets/T8.png')} style={styles.foto}/></TouchableOpacity>, nomor:8},
                {gambar:<TouchableOpacity onPress={() => this.props.navigation.navigate('Simple Future Tense')}><Image source={require('../assets/T9.png')} style={styles.foto}/></TouchableOpacity>, nomor:9},
                {gambar:<TouchableOpacity onPress={() => this.props.navigation.navigate('Future Continuous Tense')}><Image source={require('../assets/T10.png')} style={styles.foto}/></TouchableOpacity>, nomor:10},
                {gambar:<TouchableOpacity onPress={() => this.props.navigation.navigate('Future Perfect Tense')}><Image source={require('../assets/T11.png')} style={styles.foto}/></TouchableOpacity>, nomor:11},
                {gambar:<TouchableOpacity onPress={() => this.props.navigation.navigate('Future Perfect Continuous Tense')}><Image source={require('../assets/T12.png')} style={styles.foto}/></TouchableOpacity>, nomor:12},
                {gambar:<TouchableOpacity onPress={() => this.props.navigation.navigate('Simple Past Future Tense')}><Image source={require('../assets/T13.png')} style={styles.foto}/></TouchableOpacity>, nomor:13},
                {gambar:<TouchableOpacity onPress={() => this.props.navigation.navigate('Past Future Continuous Tense')}><Image source={require('../assets/T14.png')} style={styles.foto}/></TouchableOpacity>, nomor:14},
                {gambar:<TouchableOpacity onPress={() => this.props.navigation.navigate('Past Future Perfect Tense')}><Image source={require('../assets/T15.png')} style={styles.foto}/></TouchableOpacity>, nomor:15},
                {gambar:<TouchableOpacity onPress={() => this.props.navigation.navigate('Past Future Perfect Continuous Tense')}><Image source={require('../assets/T16.png')} style={styles.foto}/></TouchableOpacity>, nomor:16},
            ],
        }
    }
    render(){
        return(
            <View style = {{backgroundColor : 'black'}}>
                <FlatList
                    data = {this.state.yana}
                    renderItem = {({item,index}) => (
                        <View style={styles.ukuran}>
                            {item.gambar}
                        </View>
                    )}
                    keyExtractor = {(item) => item.nomor}
                    numColumns = {numColumns}
                />
            </View>
        )
    }
}
const styles = StyleSheet.create({
    foto: {
        width:200,
        height:200,

    },
    ukuran : {
        height: WIDTH/(numColumns * 2),
        margin: 5,
        padding: 5,
        borderRadius: 5,
        justifyContent:'center',
        alignItems:'center',
    }
})

export default vocabulary;
