import React, { Component } from 'react';
import {View, StyleSheet, Text,FlatList} from 'react-native';

class tenses1 extends Component{
    constructor(props){
        super(props);
        this.state = {
            yana : [
                {judul:<Text style={{alignSelf:'center',fontWeight:'bold',fontSize:25,marginBottom:20}}>SIMPLE PRESENT TENSE</Text>,nomor:1},
                {judul:<Text style={{marginLeft:5}}>Simple present tense adalah tenses yang berfungsi untuk menjelaskan fakta, kejadian, atau tindakan yang sedang berlangsung di masa sekarang atau berlangsung berulang kali. Kata kerja yang digunakan dalam simple present tense adalah infinitive verb atau bentuk dasar kata kerja yang menunjukkan aksi atau tindakan.</Text>,nomor:2},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>Jika subject dalam kalimat berupa singular noun (Rudy, ruler, ring) atau third person singular pronoun (she, he, it), maka verb 1 (kecuali verb to be) perlu ditambahkan -s/-es/-ies. Namun, jika subject dalam kalimat berupa pronoun I/you, plural noun (boys, women, rulers), plural pronoun (they, we), maka verb 1 tidak perlu ditambahkan -s/-es/-ies.</Text>,nomor:3},
                {judul:<Text style ={{alignSelf :'center',fontSize:20,marginTop:10}}>Rumus Simple Present Tense</Text>,nomor:4},
                {judul:<Text style = {{fontWeight:'bold', marginTop:10,marginLeft:5}}>Kalimat Positif Simple Present Tense</Text>,nomor:5},
                {judul:<Text style={{alignSelf:'center',fontWeight:'bold',marginTop:10}}>S + Verb 1 atau Be (is/am/are) + O</Text>,nomor:6},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>       Contoh kalimat:</Text>,nomor:7},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>a.  He catches a fish</Text>,nomor:8},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>b.  They are humble</Text>,nomor:9},
                {judul:<Text style={{fontWeight:'bold', marginTop:10, marginLeft:5}}>Kalimat Negatif Simple Present Tense</Text>,nomor:10},
                {judul:<Text style={{alignSelf:'center',fontWeight:'bold', marginTop:10, marginLeft:5}}>S + Auxiliary Verb (do/does) + not + Verb 1 (bentuk dasar) atau S + Be (is/am/are) + not</Text>,nomor:11},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>        Contoh kalimat:</Text>,nomor:12},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>a.  He doesn’t catch a fish</Text>,nomor:13},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>b.  They aren’t humble</Text>,nomor:14},
                {judul:<Text style={{fontWeight:'bold', marginTop:10,marginLeft:5}}>Kalimat Interogatif Simple Present Tense</Text>,nomor:15},
                {judul:<Text style={{alignSelf:'center',fontWeight:'bold', marginTop:10,marginLeft:5}}>Do/does + S + Verb 1 (bentuk dasar) atau Be (is/am/are) + S</Text>,nomor:16},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>        Contoh kalimat:</Text>,nomor:17},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>a.  Does he catch a fish?</Text>,nomor:18},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>b.  Are they humble?</Text>,nomor:19},
                {judul:<Text style={{alignSelf :'center',fontSize:20,marginTop:10}}>Verb Akhiran -s/-es/-ies</Text>,nomor:20},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>Bingung menentukan kira-kira verb yang mau digunakan memakai akhiran yang mana? Berikut penjelasannya:</Text>,nomor:21},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>Jika verb berakhiran -O, -CH, -S, -SH, -X, -Z, maka tambahkan -ES</Text>,nomor:22},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>Contoh : Go = Goes , Watch = Watches, Miss = Misses, Fix = Fixes, Frizz = Frizzes</Text>,nomor:23},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>Verb Akhiran Konsonan -Y</Text>,nomor:24},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>Jika verb berakhiran konsonan -Y, maka hilangkan dan tambahkan -IES</Text>,nomor:25},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>Contoh : Try = Tries, Cry = Cries, Fry = Fries, Worry = Worries, Carry = Carries</Text>,nomor:26},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>Verb Akhiran Vokal -Y</Text>,nomor:27},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>Jika verb berakhiran vokal -Y, maka tambahkan +s</Text>,nomor:28},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>Contoh : Play = Plays, Say = Says, Pay = Pays, Buy = Buys, Enjoy = Enjoys</Text>,nomor:29},
                {judul:<Text style={{alignSelf:'center',fontSize:20,marginTop:10}}>     Contoh Kalimat Simple Present Tense</Text>,nomor:30},
                {judul:<Text style={{marginLeft:5, marginTop:10}}>a.    Joe Biden is The President of the United States = Joe Biden adalah Presiden Amerika Serikat</Text>,nomor:31},
                {judul:<Text style={{marginLeft:5, marginTop:10}}>b.    Rudy cries a the bathroom now = Rudy sedang menangis di kamar mandi sekarang</Text>,nomor:32},
                {judul:<Text style={{marginLeft:5, marginTop:10}}>c.    He sleeps at 10 pm every day = Dia tidur jam 10 setiap hari</Text>,nomor:33},
                {judul:<Text style={{marginLeft:5, marginTop:10}}>d.    She always comes late = Dia selalu datang terlambat</Text>,nomor:34},
                {judul:<Text style={{marginLeft:5, marginTop:10}}>e.    I never cook breakfast = Saya tidak pernah memasak sarapan</Text>,nomor:35},
                {judul:<Text style={{marginLeft:5, marginTop:10}}>f.    We eat dinner together every Sunday = Kami makan malam bersama setiap hari Minggu</Text>,nomor:36},
                {judul:<Text style={{marginLeft:5, marginTop:10}}>g.    Joe Biden is not The President of Indonesia = Joe Biden bukan Presiden Republik Indonesia</Text>,nomor:37},
                {judul:<Text style={{marginLeft:5, marginTop:10}}>h.    Eits, jangan berhenti dengan membaca teorinya saja. Kamu harus latihan menggunakan simple present tense supaya lebih cepat menguasainya.</Text>,nomor:38},
                
            ]
        }
    }
    render(){
        return(
            <View>
                <FlatList
                data = {this.state.yana}
                renderItem = {({item,index}) => (
                    <View>
                        {item.judul}
                    </View>
                )}
                keyExtractor = {(item) => item.nomor}
                />
            </View>
        )
    }
}

export default tenses1;