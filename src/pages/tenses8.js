import React, { Component } from 'react';
import {View, StyleSheet, Text, FlatList} from 'react-native';

class tenses8 extends Component{
    constructor(props){
        super(props);
        this.state = {
            yana : [
                {judul:<Text style={{alignSelf:'center',fontWeight:'bold',fontSize:25,marginBottom:20}}>PAST PERFECT CONTINUOUS TENSE </Text>,nomor:1},
                {judul:<Text style={{marginLeft:5,}}>Past perfect continuous tense adalah suatu bentuk kata kerja yang digunakan untuk mengungkapkan suatu aksi (dengan durasi waktu tertentu) telah selesai pada suatu titik waktu tertentu di masa lalu. Tense ini memiliki nama lain sebagai past perfect progressive tense. banyak orang yang masih keliru dengan present perfect continuous yang menjelaskan mengenai aksi yang terjadi di masa lalu namun masih terjadi sampai sekarang. Sesuai dengan namanya yang mengandung “past”, past perfect continuous ini merupakan sebuah kejadian atau aksi yang dimulai di masa lalu, terjadi di masa lalu, dan juga selesai dalam periode waktu di masa lalu. Secara sederhana, kondisi yang dirujuk dalam penggunaan tense ini harus sudah selesai.</Text>,nomor:2},
                {judul:<Text style={{alignSelf:'center',fontSize:20,marginTop:10}}>Rumus Past Perfect Continuous Tense</Text>,nomor:3},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>Past perfect continuous tense dibentuk dengan auxiliary verb “had” dan “been” dan “present participle”. Secara umum past perfect continuous tense hanya terjadi pada aksi berupa dynamic atau action verb, tidak stative verb karena umumnya hanya dynamic verb yang memiliki bentuk continuous. Adapun rumus yang digunakan dibagi menjadi 3 sesuai dengan jenis kalimatnya. Antara lain:</Text>,nomor:4},
                {judul:<Text style={{marginLeft:5,marginTop:10, fontWeight:'bold'}}>1.      Kalimat positif</Text>,nomor:5},
                {judul:<Text style={{alignSelf:'center', fontWeight:'bold', marginTop:10}}>S + had + been + -ing/present participle. </Text>,nomor:6},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>       Contoh: </Text>,nomor:7},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>a.     Lia had been walking.</Text>,nomor:8},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>b.     The laborers had been working.</Text>,nomor:9},
                {judul:<Text style={{marginLeft:5,marginTop:10, fontWeight:'bold'}}>2.      Kalimat negatif</Text>,nomor:10},
                {judul:<Text style={{alignSelf:'center', fontWeight:'bold', marginTop:10}}>S + had + not + been + -ing/present participle ? </Text>,nomor:11},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>       Contoh: </Text>,nomor:12},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>a.     Lia had not been walking.</Text>,nomor:13},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>b.     The laborers hadn’t been working.</Text>,nomor:14},
                {judul:<Text style={{marginLeft:5,marginTop:10, fontWeight:'bold'}}>3.     Kalimat introgatif</Text>,nomor:15},
                {judul:<Text style={{alignSelf:'center', fontWeight:'bold', marginTop:10}}>Had + S + been + -ing/present participle</Text>,nomor:16},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>       Contoh: </Text>,nomor:17},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>a.     Had Lia been walking?</Text>,nomor:18},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>b.     Had the laborers been working?</Text>,nomor:19},
                {judul:<Text style={{alignSelf:'center',fontSize:20,marginTop:10}}>Fungsi dan Contoh Kalimat Penggunaan Past Perfect Continuous Tense</Text>,nomor:20},
                {judul:<Text style={{marginLeft:5,marginTop:10, fontWeight:'bold'}}>1.     Merujuk aksi yang panjang</Text>,nomor:21},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>Biasanya kondisi ini juga menjelaskan bahwa aksi tersebut terjadi sebelum aksi lainnya terjadi. Contoh:</Text>,nomor:22},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>a.     When the guests came, we had been waiting for an hour. (Ketika para tamu datang, kita telah menunggu selama satu jam.)</Text>,nomor:23},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>b.     He had been standing in front of the door for thirty minutes before it was opened. (Dia telah berdiri di depan pintu selama tiga puluh menit sebelum dibukakan.)</Text>,nomor:24},
                {judul:<Text style={{marginLeft:5,marginTop:10, fontWeight:'bold'}}>2.     Memberitahukan periode aksi</Text>,nomor:25},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>Jika ingin menjelaskan periode sebuah aksi yang terjadi di masa lampau secara spesifik, tense ini sangat tepat untuk di gunakan. Contoh:</Text>,nomor:26},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>a.     She was annoyed since she had been waiting for 2 hours at the bus station. (Dia kesal karena telah menunggu selama 2 jam di halte bus.)</Text>,nomor:27},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>b.     I had been working with my computer all day, and all I wanted to do was eat some chocolate. (Saya telah bekerja dengan komputer sepanjang hari, dan yang ingin saya lakukan adalah makan coklat.)</Text>,nomor:28},

            ]
        }
    }
    render(){
        return(
            <View>
                <FlatList
                data = {this.state.yana}
                renderItem = {({item,index}) => (
                    <View>
                        {item.judul}
                    </View>
                )}
                keyExtractor = {(item) => item.nomor}
                />
            </View>
        )
    }
}

export default tenses8;