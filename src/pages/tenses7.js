import React, { Component } from 'react';
import {View, StyleSheet, Text,FlatList} from 'react-native';

class tenses7 extends Component{
    constructor(props){
        super(props);
        this.state = {
            yana : [
                {judul:<Text style={{alignSelf:'center',fontWeight:'bold',fontSize:25,marginBottom:20}}>PAST PERFECT TENSE</Text>,nomor:1},
                {judul:<Text style={{marginLeft:5,}}>Past perfect tense adalah suatu bentuk kata kerja yang digunakan untuk menyatakan bahwa suatu aksi telah selesai pada suatu titik di masa lalu sebelum aksi lainnya terjadi. Aksi yang telah selesai di masa lampau itu dapat terjadi berulang kali maupun hanya sekali. Salah satu ciri khas dari tense ini adalah penggunaan “had” sebelum kata kerja. Selain itu, tense ini juga dapat digunakan untuk membentuk conditional sentence tipe 3 dan reported speech.</Text>,nomor:2},
                {judul:<Text style={{alignSelf:'center',fontSize:20,marginTop:10}}>Rumus Past Perfect Tense</Text>,nomor:3},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>Past perfect tense dibentuk dengan auxiliary verb “had”, dan past participle (verb-3). Had digunakan baik untuk singular maupun plural subject. Sedangkan past participle dibentuk dengan menambahkan –ed, -en, -d, -t, -n, atau -ne pada base form berupa regular verb. Pada base form berupa irregular verb, bentuk past participle tidak konsisten. Dalam past perfect, rumus dibagi menjadi 3 sesuai dengan jenis kalimatnya, antara lain:</Text>,nomor:4},
                {judul:<Text style={{marginLeft:5,marginTop:10, fontWeight:'bold'}}>1.      Kalimat positif </Text>,nomor:5},
                {judul:<Text style={{alignSelf:'center', fontWeight:'bold', marginTop:10}}>(S + had + past participle (V-3)</Text>,nomor:6},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>       Contoh: </Text>,nomor:7},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>a.     My brother had slept</Text>,nomor:8},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>b.     They had come</Text>,nomor:9}, 
                {judul:<Text style={{marginLeft:5,marginTop:10, fontWeight:'bold'}}>2.      Kalimat negatif </Text>,nomor:10},
                {judul:<Text style={{alignSelf:'center', fontWeight:'bold', marginTop:10}}>(S + had + not + past participle (V-3)</Text>,nomor:11},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>       Contoh: </Text>,nomor:12},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>a.     My brother hadn’t slept</Text>,nomor:13},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>b.     They hadn’t come</Text>,nomor:14},
                {judul:<Text style={{marginLeft:5,marginTop:10, fontWeight:'bold'}}>3.      Kalimat interogatif / kalimat tanya </Text>,nomor:15},
                {judul:<Text style={{alignSelf:'center', fontWeight:'bold', marginTop:10}}>(Had + S + past participle (V-3)</Text>,nomor:16},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>       Contoh: </Text>,nomor:17},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>a.     Had my brother slept?</Text>,nomor:18},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>b.     Had they come?</Text>,nomor:19},
                {judul:<Text style={{alignSelf:'center',fontSize:20,marginTop:10}}>Fungsi dan Penggunaan Past Perfect Tense</Text>,nomor:20},
                {judul:<Text style={{marginLeft:5,marginTop:10, fontWeight:'bold'}}>1.    Menjelaskan momen yang sudah selesai</Text>,nomor:21},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>Tense ini bisa berfungsi untuk mengekspresikan aksi di masa lampau yang telah selesai terjadi sebelum momen lampau lainnya terjadi. Jika kamu ingin membicarakan kondisi ini, kamu juga bisa menyisipkan subordinate conjunction seperti:</Text>,nomor:22},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>After (simple past tense + after + past perfect tense)</Text>,nomor:23},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>Before by the time, when (past perfect tense + before / by the time / when + simple past tense)</Text>,nomor:24},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>       Contoh:</Text>,nomor:25},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>a.     When he came last night, the cake had run out. (Ketika dia datang semalam, kue sudah habis.)</Text>,nomor:26},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>b.     I had already eaten breakfast by the time he picked me up. (Saya telah sarapan ketika dia menjemput.)</Text>,nomor:27},
                {judul:<Text style={{marginLeft:5,marginTop:10, fontWeight:'bold'}}>2.      Menunjukkan intensitas sebuah kejadian</Text>,nomor:28},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>Past perfect bisa digunakan untuk menunjukkan seberapa sering sesuatu terjadi di masa lampau. Time expression (keterangan waktu) yang sering digunakan yaitu frasa adverbial of frequency. Berikut beberapa contohnya:</Text>,nomor:29},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>Twice / two times / three times + before + simple past tense / participle (phrase): twice before getting married (dua kali sebelum menikah), two times before they got married (dua kali sebelum mereka menikah), three times before I read his review (tiga kali sebelum saya membaca tinjauannya) Every day / every two days / every other day until 2012 (setiap hari / setiap dua hari sampai tahun 2012)</Text>,nomor:30},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>       Contoh: </Text>,nomor:31},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>a.     I had read the book three times before I read his review. (Saya telah membaca buku itu tiga kali sebelum saya membaca ulasannya.)</Text>,nomor:32},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>b.     They had met twice before married. (Mereka bertemu dua kali sebelum menikah.)</Text>,nomor:33},
                {judul:<Text style={{marginLeft:5,marginTop:10}}></Text>,nomor:34},
                {judul:<Text style={{marginLeft:5,marginTop:10}}></Text>,nomor:35},
            ]
        }
    }
    render(){
        return(
            <View>
                <FlatList
                data = {this.state.yana}
                renderItem = {({item,index}) => (
                    <View>
                        {item.judul}
                    </View>
                )}
                keyExtractor = {(item) => item.nomor}
                />
            </View>
        )
    }
}

export default tenses7;