import React, { Component } from 'react';
import {View, StyleSheet, Text,FlatList} from 'react-native';

class tenses2 extends Component{
    constructor(props){
        super(props);
        this.state = {
            yana : [
                {judul:<Text style={{alignSelf:'center',fontWeight:'bold',fontSize:25,marginBottom:20}}>PRESENT CONTINUOUS TENSE</Text>,nomor:1},
                {judul:<Text style={{marginLeft:5}}>present continuous tense merupakan bentuk tense yang digunakan untuk menyatakan bahwa suatu aksi sedang berlangsung selama waktu tertentu pada waktu sekarang (present). Bentuk ini menunjukkan bahwa aksi tersebut terjadi sebelum (begins before), selama (is in progress at the present), dan berlanjut setelah (continues after) waktu atau aksi lainnya.</Text>,nomor:2},
                {judul:<Text style={{alignSelf:'center',fontSize:20}}>Rumus Present Continuous Tense</Text>,nomor:3},
                {judul:<Text style={{marginLeft:5,alignSelf:'center',fontWeight:'bold',marginTop:10}}>Bentuk positif: Subjek + be (am/is/are) + verb (-ing) + …</Text>,nomor:4},
                {judul:<Text style={{marginLeft:5,alignSelf:'center',fontWeight:'bold',marginTop:10}}>Bentuk negatif: Subjek +be (am/is/are) + not + verb (-ing) + …</Text>,nomor:5},
                {judul:<Text style={{marginLeft:5,alignSelf:'center',fontWeight:'bold',marginTop:10}}>Bentuk interogatif: Be (am/is/are) + subjek + verb (-ing) + …?</Text>,nomor:6},
                {judul:<Text style={{alignSelf:'center',fontSize:20}}>Penggunaan Tense</Text>,nomor:7},
                {judul:<Text style={{marginLeft:5}}>Sesuai yang sudah kita ketahui bahwa present continuous tense ini merupakan tense yang digunakan untuk menggambarkan suatu keadaan yang sedang berlangsung. Namun, pada penggunaannya ada beberapa kondisi yang perlu kamu ketahui.</Text>,nomor:8},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>Untuk menunjukkan suatu aksi yang sedang berlangsung dan terjadi sekarang. Contoh:</Text>,nomor:9},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>It is raining and I am waiting for my mother to pick me up.</Text>,nomor:10},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>I am writing this letter to express my feelings to you.</Text>,nomor:11},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>She is dancing in the rain.</Text>,nomor:12},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>Untuk membicarakan aksi yang terjadi sekitar waktu pembicaraan. Contoh:</Text>,nomor:13},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>Arnold is taking five classes this semester.</Text>,nomor:14},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>At this moment, my husband keeps all his money in his closet.</Text>,nomor:15},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>Untuk membicarakan rencana yang sudah pasti akan terjadi dalam waktu dekat. Contoh:</Text>,nomor:16},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>She is spending her holiday in Bangkok next week.</Text>,nomor:17},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>He is renting a whole theatre for his anniversary tonight.</Text>,nomor:18},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>My boss is promoting Stacey to be the new manager next month.</Text>,nomor:19},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>Mengingat present continuous tense tidak hanya digunakan untuk menunjukkan kejadian yang berlangsung sekarang tetapi juga masa depan yang dekat, maka penggunaan keterangan waktu sangatlah penting untuk diperhatikan. Umumnya, keterangan waktu berikut digunakan pada kalimat present continuous tense adalah:</Text>,nomor:20},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>Right now</Text>,nomor:21},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>At the moment</Text>,nomor:22},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>Today</Text>,nomor:23},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>At present</Text>,nomor:24},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>This month</Text>,nomor:25},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>This year</Text>,nomor:26},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>This week</Text>,nomor:27},
                {judul:<Text style={{alignSelf:'center',fontSize:20, marginTop:10}}>Kata Kerja yang Tidak bisa Digunakan dalam Present Continuous Tense</Text>,nomor:28},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>Tahukah kamu? Tak semua kata kerja bisa digunakan dalam tense ini, lho! Hal ini dikarenakan kata kerja tersebut menunjukkan keadaan atau situasi yang kita tidak harapkan untuk berubah dan tidak menggambarkan aksi atau tindakan. Kata kerja ini lebih dikenal dengan nama kata kerja statis (stative verbs). Berikut contoh kata kerja statis:</Text>,nomor:29},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>Kata kerja  yang menunjukkan perasaan dan emosi: love, prefer, hate, like, appreciate, want, dislike.</Text>,nomor:30},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>Kata kerja yang menunjukkan penggunaan indra: hear, taste, smell, see.</Text>,nomor:31},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>Kata kerja yang mengekspresikan pengetahuan, pendapat, dan kepercayaan: think, believe, understand, know, recognize.</Text>,nomor:32},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>Kata kerja yang menunjukkan kepemilikan: own, have, possess, belong.</Text>,nomor:33},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>Kata kerja statis lainnya: seem, be, exist, need, cost dan lain lain.</Text>,nomor:34},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>Mungkin kamu bertanya-tanya mengapa kata “love” tidak bisa digunakan sebagai kata kerja dalam tense ini? Padahal mungkin kamu sering mendengar kalimat “I’m loving it!” Secara struktur grammar, sebenarnya kalimat ini salah karena mengandung stative verbs. Namun, hal ini terhitung sebagai style saja dan bersifat informal. Kamu akan sering menemukan ini dalam pembicaraan seputar orang-orang dekat saja. Kalimat ini tak akan pernah kamu temui dalam komunikasi formal atau bisnis.</Text>,nomor:35},
            ]
        }
    }
    render(){
        return(
            <View>
                <FlatList
                data = {this.state.yana}
                renderItem = {({item,index}) => (
                    <View>
                        {item.judul}
                    </View>
                )}
                keyExtractor = {(item) => item.nomor}
                />
            </View>
        )
    }
}

export default tenses2;