import React, { Component } from 'react';
import {View, StyleSheet, Text,FlatList} from 'react-native';



class phrases4 extends Component{
    constructor(props){
        super(props);
        this.state = {
            tugas : [
                {teks:<Text style={styles.modif1}>1.    I Think it is enough for today  :   Aku rasa cukup untuk hari ini</Text>,nomor:1},
                {teks:<Text style={styles.modif2}>2.    Make a dialogue with your pairs! :  Buatlah sebuah dialog dengan pasanganmu!</Text>,nomor:2},
                {teks:<Text style={styles.modif1}>3.    What are you doing?     :    Apa yang sedang kamu lakukan?</Text>,nomor:3},
                {teks:<Text style={styles.modif2}>4.    Don't be noisy, please!     :       Tolong, jangan ribut!</Text>,nomor:4},
                {teks:<Text style={styles.modif1}>5.    Don't be late!      :       Jangan Terlambat!</Text>,nomor:5},
                {teks:<Text style={styles.modif2}>6.    Good Job!        :       Kerja bagus!</Text>,nomor:6},
                {teks:<Text style={styles.modif1}>7.    Very Nice!      :       Sangat bagus!</Text>,nomor:7},
                {teks:<Text style={styles.modif2}>8.    Don't do that!      :       Jangan lakukan itu!</Text>,nomor:8},
                {teks:<Text style={styles.modif1}>9.    Can you hear my voice?      :       Bisakah kamu mendengar suaraku?</Text>,nomor:9},
                {teks:<Text style={styles.modif2}>10.   Have you ever heard about...      :   Apakah kamu pernah mendengar tentang...</Text>,nomor:10},
                {teks:<Text style={styles.modif1}>11.   Don't talk too fast!    :   Jangan berbicara terlalu cepat!</Text>,nomor:11},
                {teks:<Text style={styles.modif2}>12.   It wasn't a big deal        :       Itu bukan masalah besar</Text>,nomor:12},
                {teks:<Text style={styles.modif1}>13.   I warning your      :       Aku memperingatkanmu</Text>,nomor:13},
                {teks:<Text style={styles.modif2}>14.   And it was super nice   :       Dan itu sangat menyenangkan</Text>,nomor:14},
                {teks:<Text style={styles.modif1}>15.   It pisses me off        :       Itu membuatku kesal</Text>,nomor:15},
                {teks:<Text style={styles.modif2}>16.   I broke up with my girlfriend       :       Aku putus dengan pacarku</Text>,nomor:16},
                {teks:<Text style={styles.modif1}>17.   When your friend passed away        :       Ketika temanmu meninggal dunia</Text>,nomor:17},
                {teks:<Text style={styles.modif2}>18.   I can try them on, right?       :       Aku bisa coba semuanyakan?</Text>,nomor:18},
                {teks:<Text style={styles.modif1}>19.   Iam just wondering      :       Aku hanya ingin tahu</Text>,nomor:19},
                {teks:<Text style={styles.modif2}>20.   I did'nt know you are here      :   Aku tak tahu kamu disini</Text>,nomor:20},
            ],
        }
    }
    render(){
        return(
            <View>
                <FlatList
                data = {this.state.tugas}
                renderItem = {({item,index}) => (
                    <View>
                        {item.teks}
                    </View>
                )}
                keyExtractor = {(item) => item.nomor}
                />
            </View>
        )
    }
};
const styles = StyleSheet.create({
    modif1:{
        fontSize:20, borderColor:'black', width:400, height:60, paddingHorizontal:10,
        borderRadius:5, marginTop:15, backgroundColor:'wheat', color:'black',
        paddingTop:10,marginHorizontal:10,
    },
    modif2:{
        fontSize:20, borderColor:'black', width:400, height:60, paddingHorizontal:10,
        borderRadius:5, marginTop:15, backgroundColor:'silver', color:'black',
        paddingTop:10,marginHorizontal:10,
    },
})
export default phrases4;