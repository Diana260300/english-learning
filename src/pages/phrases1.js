import React, { Component } from 'react';
import {View, StyleSheet, Text,FlatList} from 'react-native';



class phrases1 extends Component{
    constructor(props){
        super(props);
        this.state = {
            tugas : [
                {teks:<Text style={styles.modif1}>1.    Hello       :       Hai</Text>,nomor:1},
                {teks:<Text style={styles.modif2}>2.    How are you?     :       Apa kabar?</Text>,nomor:2},
                {teks:<Text style={styles.modif1}>3.    Nice to meet you!       :       Senang bertemu denganmu! </Text>,nomor:3},
                {teks:<Text style={styles.modif2}>4.    Whats is your name?     :       Siapa namamu?</Text>,nomor:4},
                {teks:<Text style={styles.modif1}>5.    Where do you live?      :       Dimana kamu tinggal? </Text>,nomor:5},
                {teks:<Text style={styles.modif2}>6.    Whats is your father?       :       Siapa ayahmu?</Text>,nomor:6},
                {teks:<Text style={styles.modif1}>7.    I want to intriduce my self.        :       Aku ingin memperkenalkan diri.</Text>,nomor:7},
                {teks:<Text style={styles.modif2}>8.    I live in...        :       Saya tinggal di...</Text>,nomor:8},
                {teks:<Text style={styles.modif1}>9.    He's on his way home        :       Dia sedang dijalan menuju rumah </Text>,nomor:9},
                {teks:<Text style={styles.modif2}>10.   You look beautifulwearing that dress        :       Kamu terlihat cantik memakai baju itu</Text>,nomor:10},
                {teks:<Text style={styles.modif1}>11.   Whats is yous mother job?       :       Apa pekerjaan Ibumu?</Text>,nomor:11},
                {teks:<Text style={styles.modif2}>12.   I was born in...        :       Aku lahir di...</Text>,nomor:12},
                {teks:<Text style={styles.modif1}>13.   Whats is your age?      :       Berapa umurmu?</Text>,nomor:13},
                {teks:<Text style={styles.modif2}>14.   We graduated from senior high school last year      :       Kita lulus SMA tahun lalu</Text>,nomor:14},
                {teks:<Text style={styles.modif1}>15.   Where do you study?     :       Dimana kamu sekolah?</Text>,nomor:15},
                {teks:<Text style={styles.modif2}>16.   How old are you?        :       kamu umur berapa?</Text>,nomor:16},
                {teks:<Text style={styles.modif1}>17.   Iam ... years old       :       Aku berumur ... tahun</Text>,nomor:17},
                {teks:<Text style={styles.modif2}>18.   this is my sister       :       Ini saudariku</Text>,nomor:18},
                {teks:<Text style={styles.modif1}>19.   She like to drink orange juice      :       Dia suka meminum jus jeruk</Text>,nomor:19},
                {teks:<Text style={styles.modif2}>20.    I played football with my friends        :       Aku bermain sepak bola dengan temanku</Text>,nomor:20},
                
            ],
        }
    }
    render(){
        return(
            <View>
                <FlatList
                data = {this.state.tugas}
                renderItem = {({item,index}) => (
                    <View>
                        {item.teks}
                    </View>
                )}
                keyExtractor = {(item) => item.nomor}
                />
            </View>
        )
    }
};
const styles = StyleSheet.create({
    modif1:{
        fontSize:20, borderColor:'black', width:400, height:60, paddingHorizontal:10,
        borderRadius:5, marginTop:15, backgroundColor:'wheat', color:'black',
        paddingTop:10,marginHorizontal:10,
    },
    modif2:{
        fontSize:20, borderColor:'black', width:400, height:60, paddingHorizontal:10,
        borderRadius:5, marginTop:15, backgroundColor:'silver', color:'black',
        paddingTop:10,marginHorizontal:10,
    },
})
export default phrases1;
