import React, { Component } from 'react';
import {View, StyleSheet, Text, FlatList} from 'react-native';

class tenses13 extends Component{
    constructor(props){
        super(props);
        this.state = {
            yana : [
                {judul:<Text style={{alignSelf:'center',fontWeight:'bold',fontSize:25,marginBottom:20}}>SIMPLE PAST FUTURE TENSE</Text>,nomor:1},
                {judul:<Text style={{marginLeft:5}}>Past future tense atau “future in the past” adalah suatu bentuk kata kerja untuk membicarakan masa depan dari perspektif masa lalu. Lebih spesifik, bentuk ini digunakan untuk menyatakan suatu aksi yang akan dilakukan, baik secara sukarela maupun yang direncanakan, membuat prediksi, dan membuat janji di masa depan pada saat berada di masa lalu. Tense ini sering digunakan pada reported speech (kalimat tidak langsung). Secara sederhana, tense ini bisa digunakan untuk membuat kalimat pengandaian.</Text>,nomor:2},
                {judul:<Text style={{alignSelf:'center',fontSize:20,marginTop:10}}>Rumus Past Future Tense</Text>,nomor:3},
                {judul:<Text style={{marginLeft:5,marginTop:10,fontWeight:'bold'}}>1.       Kalimat positif</Text>,nomor:4},
                {judul:<Text style={{alignSelf:'center', fontWeight:'bold', marginTop:10}}>S + would + bare infinitive atau S + was/were + going to + bare infinitive </Text>,nomor:5},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>       Contoh:</Text>,nomor:6},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>a.     He would forgive you.</Text>,nomor:7},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>b.     Lia was going to give two beautiful scarfs to her friend.</Text>,nomor:8},
                {judul:<Text style={{marginLeft:5,marginTop:10,fontWeight:'bold'}}>2.       Kalimat negatif</Text>,nomor:9},
                {judul:<Text style={{alignSelf:'center', fontWeight:'bold', marginTop:10,marginLeft:10}}>S + would + not + bare infinitive  atau S + was/were + not + going to + bare infinitive </Text>,nomor:10},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>       Contoh:</Text>,nomor:11},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>a.     She wouldn’t forgive you.</Text>,nomor:12},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>b.     Lia wasn’t going to give two beautiful scarfs to her friend.</Text>,nomor:13},
                {judul:<Text style={{marginLeft:5,marginTop:10,fontWeight:'bold'}}>3.       Kalimat interogatif</Text>,nomor:14},
                {judul:<Text style={{alignSelf:'center', fontWeight:'bold', marginTop:10}}>Would + S + bare infinitive?  atau Was/were + S + going to + bare infinitive? </Text>,nomor:15},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>       Contoh:</Text>,nomor:16},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>a.     Would he forgive you?</Text>,nomor:17},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>b.     Was Lia going to give two beautiful scarves to her friend?</Text>,nomor:18},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>   Simple past future tense dibentuk dari modal would dan bare infinitive atau dibentuk dari auxiliary verb “be” (was, were), present participle “going”, dan infinitive. Apa itu infinitive? Infinitive merupakan particle “to” + bare infinitive.</Text>,nomor:19},
                {judul:<Text style={{marginLeft:5,marginTop:10}}> Ada beberapa hal penting yang kamu perlu perhatikan dalam penggunaan tense ini, antara lain:</Text>,nomor:20},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>       Was untuk singular subject berupa singular noun (seperti: Andi, book, dan cat) dan singular pronoun (seperti: I, she, he, dan it) kecuali you.</Text>,nomor:21},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>       Were yang merupakan plural verb digunakan pada plural subject berupa plural noun (seperti: cats, people, books), plural pronoun (seperti: you, they, we, dan cats), compound subject yang menggunakan and (seperti: you and I, Andi and Susi), dan you.</Text>,nomor:22},
                {judul:<Text style={{alignSelf:'center',fontSize:20,marginTop:10}}>Fungsi dan Contoh Penggunaan Past Future Tense pada Kalimat</Text>,nomor:23},
                {judul:<Text style={{marginLeft:5,marginTop:10,fontWeight:'bold'}}>1.     Kesukarelaan</Text>,nomor:24},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>Penggunaan past future bisa digunakan untuk merujuk sebuah aksi yang dilakukan dengan cara sukarela (penggunaan would). Contoh:</Text>,nomor:25},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>a.     I knew you would prepare all the things for the meeting. (Saya tau kamu akan mempersiapkan segala hal untuk pertemuan tersebut.)</Text>,nomor:26},
                {judul:<Text style={{marginLeft:5,marginTop:10,marginTop:10}}>2.     Perencanaan</Text>,nomor:27},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>Bentuk kata kerja ini dapat digunakan untuk menyatakan aksi yang direncanakan (was/were going to). Contoh:</Text>,nomor:28},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>a.     They said that they were going to visit Lombok. (Mereka mengatakan bahwa mereka akan mengunjungi Lombok.)</Text>,nomor:29},
                {judul:<Text style={{marginLeft:5,marginTop:10,fontWeight:'bold'}}>3.     Menyatakan janji</Text>,nomor:30},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>Dalam penggunaannya, past future bisa digunakan untuk merujuk aksi yang menyatakan janji. Kalimat yang menyatakan janji biasanya menggunakan would. </Text>,nomor:31},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>a.     My uncle told me that he would come on time. (Paman saya mengatakan pada saya bahwa dia akan datang tepat waktu.)</Text>,nomor:32},
                {judul:<Text style={{marginLeft:5,marginTop:10,fontWeight:'bold'}}>3.       Prediksi</Text>,nomor:33},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>Selain untuk merujuk perencanaan, tense ini juga bisa digunakan untuk membuat prediksi atau bisa juga pengandaian. Contoh:</Text>,nomor:34},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>a.     I thought that the authorities were going to investigate all allegations of fraud. (Saya pikir otoritas itu akan menyelidiki semua dugaan penipuan.)</Text>,nomor:35},
            ]
        }
    }
    render(){
        return(
            <View>
                <FlatList
                data = {this.state.yana}
                renderItem = {({item,index}) => (
                    <View>
                        {item.judul}
                    </View>
                )}
                keyExtractor = {(item) => item.nomor}
                />
            </View>
        )
    }
}

export default tenses13;