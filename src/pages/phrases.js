import React, { Component } from 'react';
import {View, StyleSheet, Text,TouchableOpacity,Image, FlatList, Dimensions} from 'react-native';

const numColumns= 1;
const WIDTH = Dimensions.get('window').width;

class vocabulary extends Component{
    constructor(props){
        super(props);
        this.state = {
            yana : [
                {gambar:<TouchableOpacity onPress={() => this.props.navigation.navigate('Phrases 1')}><Image source={require('../assets/p1.png')} style={styles.foto}/></TouchableOpacity>, nomor:1},
                {gambar:<TouchableOpacity onPress={() => this.props.navigation.navigate('Phrases 2')}><Image source={require('../assets/p2.png')} style={styles.foto}/></TouchableOpacity>, nomor:2},
                {gambar:<TouchableOpacity onPress={() => this.props.navigation.navigate('Phrases 3')}><Image source={require('../assets/p3.png')} style={styles.foto}/></TouchableOpacity>, nomor:3},
                {gambar:<TouchableOpacity onPress={() => this.props.navigation.navigate('Phrases 4')}><Image source={require('../assets/p4.png')} style={styles.foto}/></TouchableOpacity>, nomor:4},
                {gambar:<TouchableOpacity onPress={() => this.props.navigation.navigate('Phrases 5')}><Image source={require('../assets/p5.png')} style={styles.foto}/></TouchableOpacity>, nomor:5},
            ],
        }
    }
    render(){
        return(
            <View>
                <FlatList
                    data = {this.state.yana}
                    renderItem = {({item,index}) => (
                        <View style={styles.ukuran}>
                            {item.gambar}
                        </View>
                    )}
                    keyExtractor = {(item) => item.nomor}
                    numColumns = {numColumns}
                />
            </View>
        )
    }
}
const styles = StyleSheet.create({
    foto: {
        width:400,
        height:400,
    },
    ukuran : {
        height: WIDTH/(numColumns * 3),
        margin: 5,
        padding: 5,
        borderRadius: 5,
        justifyContent:'center',
        alignItems:'center',
    }
})

export default vocabulary;
