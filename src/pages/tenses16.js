import React, { Component } from 'react';
import {View, StyleSheet, Text, FlatList} from 'react-native';

class tenses16 extends Component{
    constructor(props){
        super(props);
        this.state = {
            yana : [
                {judul:<Text style={{alignSelf:'center',fontWeight:'bold',fontSize:25,marginBottom:20}}>PAST FUTURE PERFECT CONTINUOUS TENSE</Text>,nomor:1},
                {judul:<Text style={{marginLeft:5}}>Past future perfect continuous tense kata kerja yang menerangkan suatu peristiwa atau pekerjaan yang akan sudah dan masih sedang dilakukan pada masa lalu dan tak ada hubungannya dengan masa sekarang. Waktu dari suatu peristiwa atau pekerjaan dalam past future perfect continuous tense biasanya diketahui dan terukur, selama berapa lama, dari jam berapa sampai jam berapa, dari hari apa sampai hari apa, dan ini merupakan salah satu perbedaan tense ini dengan Future perfect continuous tense.</Text>,nomor:2},
                {judul:<Text style={{alignSelf:'center',fontSize:20,marginTop:10}}>Rumus Past Future Perfect Continuous Tense </Text>,nomor:3},
                {judul:<Text style={{marginLeft:5, marginTop:10,fontWeight:'bold'}}>1.        Kalimat Positif</Text>,nomor:4},
                {judul:<Text style={{alignSelf:'center', fontWeight:'bold', marginTop:10}}>S + would + have + been + present participle (V1-ing)</Text>,nomor:5},
                {judul:<Text style={{marginLeft:5, marginTop:10}}>      Contoh Kalimat:</Text>,nomor:6},
                {judul:<Text style={{marginLeft:5, marginTop:10}}>a.    I would have been standing here.</Text>,nomor:7},
                {judul:<Text style={{marginLeft:5, marginTop:10,fontWeight:'bold'}}>2.      Kalimat Negatif</Text>,nomor:8},
                {judul:<Text style={{alignSelf:'center', fontWeight:'bold', marginTop:10}}>S + would + have + not + been + present participle (V1-ing)</Text>,nomor:9},
                {judul:<Text style={{marginLeft:5, marginTop:10}}>      Contoh Kalimat:</Text>,nomor:10},
                {judul:<Text style={{marginLeft:5, marginTop:10}}>a.    He would not have been living in this city.</Text>,nomor:11},
                {judul:<Text style={{marginLeft:5, marginTop:10,fontWeight:'bold'}}>3.      Kalimat Introgatif</Text>,nomor:12},
                {judul:<Text style={{alignSelf:'center', fontWeight:'bold', marginTop:10}}>Would + s + have + been + present participle(V1-ing) ?</Text>,nomor:13},
                {judul:<Text style={{marginLeft:5, marginTop:10}}>      Contoh Kalimat:</Text>,nomor:14},
                {judul:<Text style={{marginLeft:5, marginTop:10}}>1.    Would they have been running?</Text>,nomor:15},
                {judul:<Text style={{alignSelf:'center',fontSize:20,marginTop:10}}>Fungsi Past Future Perfect Continuous Tense</Text>,nomor:16},
                {judul:<Text style={{marginLeft:5, marginTop:10}}>Beberapa fungsi past future perfect continuous tense dapat digunakan sebagai berikut:</Text>,nomor:17},
                {judul:<Text style={{marginLeft:5, marginTop:10,fontWeight:'bold'}}>1.      Menyatakan akan lamanya suatu peristiwa, tindakan dan perbuatan yang akan sudah sedang terjadi pada masa lampau.</Text>,nomor:18},
                {judul:<Text style={{marginLeft:5, marginTop:10}}>      Contoh Kalimat:</Text>,nomor:19},
                {judul:<Text style={{marginLeft:5, marginTop:10}}>a.    I should have been living here for five years by last June. (Aku akan sudah sedang tinggal di sini selama lima tahun menjelang akhir bulan Juni.)</Text>,nomor:20},
                {judul:<Text style={{marginLeft:5, marginTop:10,fontWeight:'bold'}}>2.      Menyatakan suatu peristiwa, tindakan dan perbuatan yang waktunya sedang berlangsung sudah terjadi dan akan berlangsung di masa lampau.</Text>,nomor:21},
                {judul:<Text style={{marginLeft:5, marginTop:10}}>      Contoh Kalimat:</Text>,nomor:22},
                {judul:<Text style={{marginLeft:5, marginTop:10}}>a.    By last July, I should have been working in this office for three years. (Menjelang bulan Juli yang lalu aku akan sudah sedang bekerja di kantor ini selama tiga tahun.)</Text>,nomor:23},
                {judul:<Text style={{alignSelf:'center',fontSize:20,marginTop:10}}>Ciri-Ciri Past Future Perfect Continuous Tense</Text>,nomor:24},
                {judul:<Text style={{marginLeft:5, marginTop:10}}>Ciri-ciri past future perfect continuous tense adalah sebagai berikut :</Text>,nomor:25},
                {judul:<Text style={{marginLeft:5, marginTop:10,fontWeight:'bold'}}>1.  Predikat kalimat yang sering dipakai dalam bentuk tense ini sering berupa have been, Kata have tidak mengalami perubahan walaupun kalimat tersebut menunjukkan kalimat ketiga tunggal. Dan setelah kata have diikuti oleh kata been yang berarti sudah sedang.</Text>,nomor:26},
                {judul:<Text style={{marginLeft:5, marginTop:10}}>      Contoh-contoh kalimat past future perfect continuous tense dalam bentuk positif, negatif dan interogatif adalah :</Text>,nomor:27},
                {judul:<Text style={{marginLeft:5, marginTop:10}}>a.    I would have been waiting for your e-mail for five days by the end of this week. (Aku akan sudah sedang menunggu e-mailmu selama lima hari menjelang akhir minggu ini.)</Text>,nomor:29},
                {judul:<Text style={{marginLeft:5, marginTop:10}}>b.    He would not be sleeping here. (Dia (lk) tidak akan sedang tidur di sini.)</Text>,nomor:30},
                {judul:<Text style={{marginLeft:5, marginTop:10}}>c.    Would she have been studying by last year ?</Text>,nomor:31},
                {judul:<Text style={{marginLeft:5, marginTop:10}}>  Past future perfect continuous tense adalah tense untuk menjelaskan waktu yang akan sudah sedang berlansung pada waktu lampau. Rumusnya adalah : S + would + have + not + been + present participle (V1-ing), dengan keterangan waktu yang sering dipergunakan yaitu: by last, by last week, by last month, by last year, by the end of, by the end of this week, by the end of this year.</Text>,nomor:32},
                

            ]

        }
    }
    render(){
        return(
            <View>
                <FlatList
                data = {this.state.yana}
                renderItem = {({item,index}) => (
                    <View>
                        {item.judul}
                    </View>
                )}
                keyExtractor = {(item) => item.nomor}
                />
            </View>
        )
    }
}

export default tenses16;