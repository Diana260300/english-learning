import React, { Component } from 'react';
import {View, StyleSheet, Text,FlatList} from 'react-native';



class phrases2 extends Component{
    constructor(props){
        super(props);
        this.state = {
            tugas : [
                {teks:<Text style={styles.modif1}>1.    Throw your rubbish there!    :   Buang sampahmu disana!</Text>,nomor:1},
                {teks:<Text style={styles.modif2}>2.    I forgot to clean my bedroom    :   Aku lupa membersihkan kamarku</Text>,nomor:2},
                {teks:<Text style={styles.modif1}>3.    I always wash my hair one in two days   :   Aku selalu keramas setiap dua kali sehari</Text>,nomor:3},
                {teks:<Text style={styles.modif2}>4.    My sister is playing with her phone all day long    :   Saudara perempuanku memainkan ponselnya seharian</Text>,nomor:4},
                {teks:<Text style={styles.modif1}>5.    My aunt asked me to babysit my little cousin    :   Bibiku memintaku untuk mengasuh adik sepupuku</Text>,nomor:5},
                {teks:<Text style={styles.modif2}>6.    Sintia invited mw over for dinner   :   Sintia mengundangku ke rumah untuk makan malam</Text>,nomor:6},
                {teks:<Text style={styles.modif1}>7.    I cut my fingers in the process of chopping onions : Jariku tidak sengaja teriris saat mengiris bawang</Text>,nomor:7},
                {teks:<Text style={styles.modif2}>8.    I forgot to bring my homework to school today   :   Saya lupa membawa PR saya ke sekolah hari ini</Text>,nomor:8},
                {teks:<Text style={styles.modif1}>9.    My alarm wakes me up every morning  :   Alarm ku membangunkanku setiap pagi</Text>,nomor:9},
                {teks:<Text style={styles.modif2}>10.   My mom is a teacher, she teaches...  :   Ibuku adalah guru, Dia mengajar...</Text>,nomor:10},
                {teks:<Text style={styles.modif1}>11.   I check on my social media's everyday   :   Aku mengecek sosial mediaku setiap hari</Text>,nomor:11},
                {teks:<Text style={styles.modif2}>12.   I interact with many people at school   :   Aku berinteraksi dengan banyak orang di sekolah</Text>,nomor:12},
                {teks:<Text style={styles.modif1}>13.   I feed my cat two times a day   :   Aku memberi makan kucingku dua kali sehari</Text>,nomor:13},
                {teks:<Text style={styles.modif2}>14.   I love eating at canteen when I get hungry    :   Aku suka makan di kantin ketika aku lapar</Text>,nomor:14},
                {teks:<Text style={styles.modif1}>15.   I get dressed after I take a bath   :   Aku berpakaian setelah mandi</Text>,nomor:15},
                {teks:<Text style={styles.modif2}>16.   My Brother took out the trash this morning  :   Saudaraku membuang sampah tadi pagi</Text>,nomor:16},
                {teks:<Text style={styles.modif1}>17.   I drink coffee everyday     :   Aku minum kopi setiap hari</Text>,nomor:17},
                {teks:<Text style={styles.modif2}>18.   I had Breakfast before go to school     :    Aku sarapan sebelum pergi ke sekolah</Text>,nomor:18},
                {teks:<Text style={styles.modif1}>19.   I wash my hands after eating    :   Aku mencuci tanganku setelah makan</Text>,nomor:19},
                {teks:<Text style={styles.modif2}>20.    My Grandfather is cooking traditional food  :   Kakekku sedang memasak makanan tradisional</Text>,nomor:20},
            ],
        }
    }
    render(){
        return(
            <View>
                <FlatList
                data = {this.state.tugas}
                renderItem = {({item,index}) => (
                    <View>
                        {item.teks}
                    </View>
                )}
                keyExtractor = {(item) => item.nomor}
                />
            </View>
        )
    }
};
const styles = StyleSheet.create({
    modif1:{
        fontSize:20, borderColor:'black', width:400, height:60, paddingHorizontal:10,
        borderRadius:5, marginTop:15, backgroundColor:'wheat', color:'black',
        paddingTop:10,marginHorizontal:10,
    },
    modif2:{
        fontSize:20, borderColor:'black', width:400, height:60, paddingHorizontal:10,
        borderRadius:5, marginTop:15, backgroundColor:'silver', color:'black',
        paddingTop:10,marginHorizontal:10,
    },
})
export default phrases2;