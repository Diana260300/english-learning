import React, { Component } from 'react';
import {View, StyleSheet, Text,FlatList} from 'react-native';

class tenses3 extends Component{
    constructor(props){
        super(props);
        this.state = {
            yana : [
                {judul:<Text style={{alignSelf:'center',fontWeight:'bold',fontSize:25,marginBottom:20}}>PRESENT PERFECT TENSE</Text>,nomor:1},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>Present perfect tense adalah suatu bentuk kata kerja yang digunakan untuk menyatakan suatu aksi atau situasi yang telah dimulai di masa lalu dan masih berlanjut sampai sekarang atau telah selesai pada suatu titik waktu tertentu di masa lalu namun efeknya masih berlanjut. tense ini biasanya digunakan untuk membicarakan mengenai pengalaman atau perubahan yang melibatkan sebuah tempat.</Text>,nomor:2},
                {judul:<Text style={{alignSelf:'center',fontSize:20,marginTop:10}}>Rumus Present Perfect Tense</Text>,nomor:3},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>Present perfect tense dibentuk dengan auxiliary verb have atau has, dan past participle (verb-3). Have digunakan untuk I, you, dan plural subject seperti:</Text>,nomor:4},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>Plural pronoun (they, we)</Text>,nomor:5},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>Plural noun (boys, men)</Text>,nomor:6},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>Compound subject dengan kata hubung “and” (you and I, Tom and Jack) Sedangkan has untuk singular subject, seperti:</Text>,nomor:7},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>Third-person singular pronoun (he, she, it)</Text>,nomor:8},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>Singular noun (Tom, man).</Text>,nomor:9},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>Past participle dibentuk dengan menambahkan -ed, -en, -d, -t, -n, atau -ne pada base form berupa regular verb. Pada base form berupa irregular verb, bentuk past participle tidak konsisten.</Text>,nomor:10},
                {judul:<Text style={{marginLeft:5,marginTop:10,fontWeight:'bold'}}>1.       Kalimat pernyataan positif</Text>,nomor:11},
                {judul:<Text style={{alignSelf:'center', fontWeight:'bold', marginTop:10}}>I/You/We/They + have + verb 3 ( They have lived in Jakarta for a long time)</Text>,nomor:12},
                {judul:<Text style={{alignSelf:'center', fontWeight:'bold', marginTop:10}}>He/She/It + has + verb 3 (He has learned English for one year)</Text>,nomor:13},
                {judul:<Text style={{marginLeft:5,marginTop:10,fontWeight:'bold'}}>2.       Kalimat pernyataan negatif</Text>,nomor:14},
                {judul:<Text style={{alignSelf:'center', fontWeight:'bold', marginTop:10}}> I/You/We/They + have + not + verb 3 (They have not lived in Jakarta for a long time)</Text>,nomor:15},
                {judul:<Text style={{alignSelf:'center', fontWeight:'bold', marginTop:10}}>He/She/It + has + not + verb 3 (He has not learned English for one year)</Text>,nomor:16},
                {judul:<Text style={{marginLeft:5,marginTop:10,fontWeight:'bold'}}>3.       Kalimat tanya/introgatif</Text>,nomor:17},
                {judul:<Text style={{alignSelf:'center', fontWeight:'bold', marginTop:10}}>Have + I/You/We/They + verb 3? (Have they lived in Jakarta for a long time?)</Text>,nomor:18},
                {judul:<Text style={{alignSelf:'center', fontWeight:'bold', marginTop:10}}>Has + He/She/It + verb 3? (Has he learned English for one year?)</Text>,nomor:19},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>   Berikut adalah beberapa adverbs yang biasa digunakan untuk mengekspresikan present perfect tense:</Text>,nomor:20},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>a. Yet</Text>,nomor:21},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>b. Already</Text>,nomor:22},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>c. Just</Text>,nomor:23},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>d. Ever</Text>,nomor:24},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>e. Never</Text>,nomor:25},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>   Dan ketika kita berbicara dengan tindakan-tindakan yang belum selesai atau sebuah situasi tertentu, maka kita bisa mengunakan:</Text>,nomor:26},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>a. For</Text>,nomor:27},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>b. Since</Text>,nomor:28},
                {judul:<Text style={{alignSelf:'center',fontSize:20,marginTop:10}}>Waktu yang Tepat untuk Menggunakan Present Perfect Tense</Text>,nomor:29},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>Bagaimana mengidentifikasi sebuah kondisi bahwa sebuah kalimat memiliki bentuk present perfect tense? Atau kapan saat yang tepat menggunakan tense ini? Kamu bisa melihat ciri-cirinya dengan contoh keterangan waktu alias time expression berikut ini.</Text>,nomor:30},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>a. Already</Text>,nomor:31},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>b. Just</Text>,nomor:32},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>c. Recently/lately</Text>,nomor:33},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>d. Ever</Text>,nomor:34},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>e. Yet</Text>,nomor:35},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>f. For + keterangan waktu yang menunjukkan masa lalu hingga sekarang</Text>,nomor:36},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>g. Since + keterangan waktu yang menujukkan pertama kali kejadian atau kegiatan dimulai</Text>,nomor:37},
                {judul:<Text style={{alignSelf:'center',fontSize:20,marginTop:10}}>Contoh Kalimat Present Perfect Tense</Text>,nomor:38},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>1.      Membicarakan pengalaman</Text>,nomor:39},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>a. I have been to France.</Text>,nomor:40},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>b. I think I have seen that movie before.</Text>,nomor:41},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>2.      Sebuah perubahan</Text>,nomor:42},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>a. You have grown since the last time I saw you.</Text>,nomor:43},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>b. The government has become more interested in arts education.</Text>,nomor:44},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>3.     Pencapaian atau hasil</Text>,nomor:45},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>a. Our son has learned how to read.</Text>,nomor:46},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>b. Doctors have cured many deadly diseases.</Text>,nomor:47},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>4.     Aktivitas yang belum terselesaikan</Text>,nomor:48},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>a. James has not finished his homework yet.</Text>,nomor:49},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>b  The rain hasn’t stopped.</Text>,nomor:50},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>5.     Beberapa aktivitas yang terjadi dalam kurun waktu yang berbeda</Text>,nomor:51},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>a. The army has attacked that city five times.</Text>,nomor:52},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>b. We have had many major problems while working on this project.</Text>,nomor:53},

            ]
        }
    }
    render(){
        return(
            <View>
                <FlatList
                data = {this.state.yana}
                renderItem = {({item,index}) => (
                    <View>
                        {item.judul}
                    </View>
                )}
                keyExtractor = {(item) => item.nomor}
                />
            </View>
        )
    }
}

export default tenses3;