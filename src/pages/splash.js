import React, {useEffect} from 'react';
import {StyleSheet, View,Image} from 'react-native';

const Splash = ({navigation}) => {
    useEffect(() => {
        setTimeout(() => {
            navigation.replace('Login');
        }, 5000);
    });
    return (
        <View style={styles.wrapper}>
            <Image source={require('../assets/ikon1.png')} style={{width:270,height:270}}/>
        </View>
    );
};

export default Splash;

const styles=StyleSheet.create({
    wrapper: {
        flex:1,
        backgroundColor: 'black',
        alignItems: 'center',
        justifyContent: 'center'
    },
    logo: {
        margin: 10,
    },
});