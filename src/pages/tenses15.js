import React, { Component } from 'react';
import {View, StyleSheet, Text, FlatList} from 'react-native';

class tenses15 extends Component{
    constructor(props){
        super(props);
        this.state = {
            yana : [
                {judul:<Text style={{alignSelf:'center',fontWeight:'bold',fontSize:25,marginBottom:20}}>PAST FUTURE PERFECT TENSE</Text>,nomor:1},
                {judul:<Text style={{marginLeft:5}}>    Past future perfect tense adalah bentuk kalimat yang berfungsi untuk mengekspresikan gagasan di masa lalu yang mana bahwa suatu tindakan/peristiwa yang diprediksi, direncanakan, dijanjikan, diharapkan atau telah dilakukan sebelum waktu tertentu di masa depan dari sudut pandang masa lalu tetapi faktanya bahwa gagasan itu belum terbukti benar atau gagasan itu tidak terlaksana sesuai dengan yang diinginkan. Dalam tata Bahasa Inggris, bentuk tense ini bisa juga di gunakan untuk menggambarkan dan menunjukkan suatu kemungkinan kejadian atau keadaan di masa lampau, contohnya : “The girl looks sad. She may have waited for his boyfriend for hours”.</Text>,nomor:2},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>   Past future perfect tense terdiri dari tiga bentuk kalimat lain, yaitu simple past tense (bentuk lampau) menggunakan Verb 2, simple future tense (bentuk masa akan datang) yang dibentuk dengan rumus S + will + Verb-1 dan present perfect tense, yang dibangun dengan bentuk Have + Verb-3. Bentuk rumus past future perfect tense adalah bentuk kedua (verb-2) dari will yaitu would ditambah have + Verb-3 menjadikan bentuk rumusnaya yaitu : S + would + have + verb-3.</Text>,nomor:3},
                {judul:<Text style={{alignSelf:'center',fontSize:20,marginTop:10}}>Rumus past future perfect tense</Text>,nomor:4},
                {judul:<Text style={{marginLeft:5,marginTop:10,fontWeight:'bold'}}>1.       Rumus Kalimat Positif</Text>,nomor:5},
                {judul:<Text style={{alignSelf:'center', fontWeight:'bold', marginTop:10}}>S + would/should + have + V-3/past participle</Text>,nomor:6},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>       Contoh Kalimat:</Text>,nomor:7},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>a.     She would have seen the car accident on the street.</Text>,nomor:8},
                {judul:<Text style={{marginLeft:5,marginTop:10,fontWeight:'bold'}}>2.       Rumus Kalimat Negatif</Text>,nomor:9},
                {judul:<Text style={{alignSelf:'center', fontWeight:'bold', marginTop:10}}>S + would/should + not + have + V-3/past participle</Text>,nomor:10},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>       Contoh Kalimat:</Text>,nomor:11},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>a.     We should not have read this information.</Text>,nomor:12},
                {judul:<Text style={{marginLeft:5,marginTop:10,fontWeight:'bold'}}>3.       Rumus Kalimat Interogatif</Text>,nomor:13},
                {judul:<Text style={{alignSelf:'center', fontWeight:'bold', marginTop:10}}>Would/should + S + have + V-3/past participle?</Text>,nomor:14},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>       Contoh Kalimat:</Text>,nomor:15},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>1.     Would She have seen the car accident on the street?.</Text>,nomor:16},
                {judul:<Text style={{alignSelf:'center',fontSize:20,marginTop:10}}>Fungsi Past Future Perfect Tense</Text>,nomor:17},
                {judul:<Text style={{marginLeft:5,marginTop:10,fontWeight:'bold'}}>1.     Berfungsi untuk menyatakan suatu peristiwa atau kejadian yang akan sudah dilakukan pada masa lampau.</Text>,nomor:18},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>       Contoh Kalimat:</Text>,nomor:19},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>a.     Michael would have done his history assignment last night. (Michael seharusnya sudah mengerjakan tugas sejarahnya tadi malam).</Text>,nomor:20},
                {judul:<Text style={{marginLeft:5,marginTop:10,fontWeight:'bold'}}>2.   Berfungsi untuk membuat kalimat pengandaian (conditional sentence) yang menyatakan pengandaian yang sudah pasti tidak mungkin terjadi, karena menunjukkan suatu peristiwa, tindakan dan perbuatan yang lampau.</Text>,nomor:21},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>       Contoh Kalimat:</Text>,nomor:22},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>a.     If you had met Sally, you would have felt happy last week. (Seadainya kamu telah menemui Sally, kamu akan sudah merasa bahagia minggu lalu).</Text>,nomor:23},
                {judul:<Text style={{marginLeft:5,marginTop:10,fontWeight:'bold'}}>3.   Berfungsi untuk menceritakan suatu kejadian di masa lampau yang tidak sukses dilakukan, dengan menggunakan “could“.</Text>,nomor:24},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>       Contoh Kalimat:</Text>,nomor:25},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>a.     Anto could have bought new house, but he prefered to save money and wait for the new car.</Text>,nomor:26},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>If clause yang berarti anak kalimat yang berisi syarat bisa diletakkan di awal atau di tengah kalimat. Sedangkan Main clause yang berarti induk kalimat yang berisi akibat bisa pula diletakkan di awal atau di tengah kalimat.</Text>,nomor:27},
                {judul:<Text style={{alignSelf:'center',fontSize:20,marginTop:10}}>Ciri-ciri Past Future Perfect Tense</Text>,nomor:28},
                {judul:<Text style={{marginLeft:5,marginTop:10,fontWeight:'bold'}}>1.       Predikat kalimat yang sering dipakai dalam bentuk tense ini sering berupa: S + Should/Would + Have + Past Participle.</Text>,nomor:29},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>       Contoh Kalimat:</Text>,nomor:30},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>a.     I should have watched the movie last week. (Aku akan sudah menonton film itu minggu lalu).</Text>,nomor:31},
                {judul:<Text style={{marginLeft:5,marginTop:10,fontWeight:'bold'}}>2.       Kata have diletakkan sebelah kata kerja bantu should/would serta ditambah present participle yang berarti sudah.</Text>,nomor:32},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>       Contoh Kalimat:</Text>,nomor:33},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>a.     Samantha would have performed drama last year. (Samantha akan sudah melakukan pertunjukan drama tahun lalu).</Text>,nomor:34},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>   Past future perfect tense adalah bentuk kalimat yang memiliki fungsi untuk mengekspresikan gagasan di masa depan dari sudut pandang masa lalu tetapi faktanya bahwa gagasan itu tidak terbukti.</Text>,nomor:35},
                {judul:<Text style={{marginLeft:5,marginTop:10}}>   Bentuk rumus tense ini adalah S + would/should + not + have + V-3/past participle dengan ciri-ciri predikat kalimatnya yang sering dipakai adalah: S + Should/Would + Have + Past Participle dan keterangan waktu on last week atau at one o’ clock yesterday.</Text>,nomor:36},
            ]
        }
    }
    render(){
        return(
            <View>
                <FlatList
                data = {this.state.yana}
                renderItem = {({item,index}) => (
                    <View>
                        {item.judul}
                    </View>
                )}
                keyExtractor = {(item) => item.nomor}
                />
            </View>
        )
    }
}

export default tenses15;