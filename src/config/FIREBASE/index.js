import firebase from 'firebase';

var firebaseConfig = {
    apiKey: "AIzaSyCBNimZGCAVdNoSqCHYD-kqXUk0JwujItE",
  authDomain: "english-11b94.firebaseapp.com",
  databaseURL: "https://english-11b94-default-rtdb.firebaseio.com",
  projectId: "english-11b94",
  storageBucket: "english-11b94.appspot.com",
  messagingSenderId: "777349539600",
  appId: "1:777349539600:web:ad08b99e0c3e87263c6795",
  measurementId: "G-JLQ31919ZL"
  };

  firebase.initializeApp(firebaseConfig);

  const FIREBASE = firebase;

  export default FIREBASE;