import React, {Component} from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import Splash from '../pages/splash';
import Login from '../pages/Login';
import WelcomeAuth from '../pages/welcome';
import Vocabulary from '../pages/vocabulary';
import Phrases from '../pages/phrases';
import Tenses from '../pages/tenses';
import Vocabulary1 from '../pages/vocabulary1';
import Vocabulary2 from '../pages/vocabulary2';
import Vocabulary3 from '../pages/vocabulary3';
import Vocabulary4 from '../pages/vocabulary4';
import Phrases1 from '../pages/phrases1';
import Phrases2 from '../pages/phrases2';
import Phrases3 from '../pages/phrases3';
import Phrases4 from '../pages/phrases4';
import Phrases5 from '../pages/phrases5';
import Tenses1 from '../pages/tenses1';
import Tenses2 from '../pages/tenses2';
import Tenses3 from '../pages/tenses3';
import Tenses4 from '../pages/tenses4';
import Tenses5 from '../pages/tenses5';
import Tenses6 from '../pages/tenses6';
import Tenses7 from '../pages/tenses7';
import Tenses8 from '../pages/tenses8';
import Tenses9 from '../pages/tenses9';
import Tenses10 from '../pages/tenses10';
import Tenses11 from '../pages/tenses11';
import Tenses12 from '../pages/tenses12';
import Tenses13 from '../pages/tenses13';
import Tenses14 from '../pages/tenses14';
import Tenses15 from '../pages/tenses15';
import Tenses16 from '../pages/tenses16';

const Stack = createStackNavigator();

const Route = () => {
    return(
        <Stack.Navigator>
            <Stack.Screen
                name="splash"
                component={Splash}
                options={{headerShown: false}}
             />
             <Stack.Screen
                name="Login"
                component={Login}
                options={{headerShown: false}}
             />
             <Stack.Screen
                name="welcome"
                component={WelcomeAuth}
                options={{headerShown: false}}
             />
             <Stack.Screen
                name="Vocabulary"
                component={Vocabulary}
             />
             <Stack.Screen
                name="Phrases"
                component={Phrases}
             />
             <Stack.Screen
                name="Tenses"
                component={Tenses}
             />
             <Stack.Screen
                name="Vocabulary 1"
                component={Vocabulary1}
             />
             <Stack.Screen
                name="Vocabulary 2"
                component={Vocabulary2}
             />
             <Stack.Screen
                name="Vocabulary 3"
                component={Vocabulary3}
             />
             <Stack.Screen
                name="Vocabulary 4"
                component={Vocabulary4}
             />
             <Stack.Screen
                name="Phrases 1"
                component={Phrases1}
             />
             <Stack.Screen
                name="Phrases 2"
                component={Phrases2}
             />
             <Stack.Screen
                name="Phrases 3"
                component={Phrases3}
             />
             <Stack.Screen
                name="Phrases 4"
                component={Phrases4}
             />
             <Stack.Screen
                name="Phrases 5"
                component={Phrases5}
             />
             <Stack.Screen
                name="Simple Present Tense"
                component={Tenses1}
             />
             <Stack.Screen
                name="Present Continuous Tense"
                component={Tenses2}
             />
             <Stack.Screen
                name="Present Perfect Tense"
                component={Tenses3}
             />
             <Stack.Screen
                name="Present Perfect Continuous Tense"
                component={Tenses4}
             />
             <Stack.Screen
                name="Simple Past Tense"
                component={Tenses5}
             />
             <Stack.Screen
                name="Past Continuous Tense"
                component={Tenses6}
             />
             <Stack.Screen
                name="Past Perfect Tense"
                component={Tenses7}
             />
             <Stack.Screen
                name="Past Perfect Continuous Tense"
                component={Tenses8}
             />
             <Stack.Screen
                name="Simple Future Tense"
                component={Tenses9}
             />
             <Stack.Screen
                name="Future Continuous Tense"
                component={Tenses10}
             />
             <Stack.Screen
                name="Future Perfect Tense"
                component={Tenses11}
             />
             <Stack.Screen
                name="Future Perfect Continuous Tense"
                component={Tenses12}
             />
             <Stack.Screen
                name="Simple Past Future Tense"
                component={Tenses13}
             />
             <Stack.Screen
                name="Past Future Continuous Tense"
                component={Tenses14}
             />
             <Stack.Screen
                name="Past Future Perfect Tense"
                component={Tenses15}
             />
             <Stack.Screen
                name="Past Future Perfect Continuous Tense"
                component={Tenses16}
             />
             
















        </Stack.Navigator>
    );
};

export default Route;